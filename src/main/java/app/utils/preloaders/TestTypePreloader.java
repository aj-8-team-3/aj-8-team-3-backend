package app.utils.preloaders;

import app.models.TestType;
import app.repositories.TestTypeRepository;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public final class TestTypePreloader implements CommandLineRunner {

    private final TestTypeRepository testTypeRepository;
    private final UUIDGenerator generator;
    private final Logger logger;


    public TestTypePreloader(TestTypeRepository testTypeRepository, UUIDGenerator generator, Logger logger) {
        this.testTypeRepository = testTypeRepository;
        this.generator = generator;
        this.logger = logger;
    }

    @Override
    public void run(String... args) throws Exception {


        if (!testTypeRepository.existsByTitle("TOEFL")) {

            TestType toefl = new TestType();
            toefl.setTitle("TOEFL");
            toefl.setSubscriptionPrice(1000);
            toefl.setInstantPrice(500);
            toefl.setId(generator);
            toefl.setIsMultiLang(false);

            testTypeRepository.save(toefl);
            logger.info("Вид теста TOEFL сохранен в базу");
        }

        if (!testTypeRepository.existsByTitle("ЕНТ")) {

            TestType ent = new TestType();
            ent.setTitle("ЕНТ");
            ent.setSubscriptionPrice(1500);
            ent.setInstantPrice(750);
            ent.setId(generator);
            ent.setIsMultiLang(true);

            testTypeRepository.save(ent);
            logger.info("Вид теста ЕНТ сохранен в базу");

        }

        if (!testTypeRepository.existsByTitle("ОЗП")) {

            TestType ozp = new TestType();
            ozp.setTitle("ОЗП");
            ozp.setSubscriptionPrice(2000);
            ozp.setInstantPrice(1000);
            ozp.setId(generator);
            ozp.setIsMultiLang(true);

            testTypeRepository.save(ozp);
            logger.info("Вид теста ОЗП сохранен в базу");
        }
    }
}