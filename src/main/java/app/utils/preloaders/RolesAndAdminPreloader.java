package app.utils.preloaders;

import app.models.*;
import app.repositories.*;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(1)
public class RolesAndAdminPreloader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final UserAuthorityRepository userAuthorityRepository;
    private final PasswordEncoder passwordEncoder;
    private final UUIDGenerator generator;
    private final Logger logger;

    public RolesAndAdminPreloader(UserRepository userRepository,
                                  AuthorityRepository authorityRepository,
                                  UserAuthorityRepository userAuthorityRepository,
                                  PasswordEncoder passwordEncoder,
                                  UUIDGenerator generator, Logger logger) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.userAuthorityRepository = userAuthorityRepository;
        this.passwordEncoder = passwordEncoder;
        this.generator = generator;
        this.logger = logger;
    }

    @Override
    public void run(String... args) throws Exception {

        if(!authorityRepository.existsByAuthority("ROLE_SUPER_ADMIN")) {
            Authority roleSuperAdmin = new Authority();
            roleSuperAdmin.setAuthority("ROLE_SUPER_ADMIN");
            roleSuperAdmin.setId(generator);
            authorityRepository.save(roleSuperAdmin);
            logger.info("Роль SUPER_ADMIN загружена в базу");
        }

        if(!authorityRepository.existsByAuthority("ROLE_ADMIN")) {
            Authority roleAdmin = new Authority();
            roleAdmin.setAuthority("ROLE_ADMIN");
            roleAdmin.setId(generator);
            authorityRepository.save(roleAdmin);
            logger.info("Роль ADMIN загружена в базу");

        }

        if(!authorityRepository.existsByAuthority("ROLE_USER")) {
            Authority roleUser = new Authority();
            roleUser.setAuthority("ROLE_USER");
            roleUser.setId(generator);
            authorityRepository.save(roleUser);
            logger.info("Роль USER загружена в базу");

        }

        if (!userRepository.existsByIdentifier("super-admin@gmail.com")) {
            User admin = new User();
            admin.setId(generator);
            admin.setIdentifier("super-admin@gmail.com");
            admin.setPassword(passwordEncoder.encode("super-admin"));
            admin.setVerified(true);
            userRepository.save(admin);

            logger.info("Сущность super-admin создана и загружена в базу");

            List<Authority> authorities = authorityRepository.findAll();

            UserAuthority userAuthority = new UserAuthority();
            userAuthority.setId(generator);
            userAuthority.setUserId(admin.getId());
            userAuthority.setAuthorities(authorities);
            userAuthorityRepository.save(userAuthority);

            logger.info("Сущности super-admin присвоены все существующие права");
        }
    }
}