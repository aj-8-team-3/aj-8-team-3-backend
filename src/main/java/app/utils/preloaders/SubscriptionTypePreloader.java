package app.utils.preloaders;

import app.configuration.constants.SubscriptionDurationAndPriceConstants;
import app.models.*;
import app.repositories.*;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class SubscriptionTypePreloader implements CommandLineRunner {

    private final SubscriptionTypeRepository subscriptionTypeRepository;
    private final UUIDGenerator generator;
    private final Logger logger;

    public SubscriptionTypePreloader(SubscriptionTypeRepository subscriptionTypeRepository, UUIDGenerator generator, Logger logger) {
        this.subscriptionTypeRepository = subscriptionTypeRepository;
        this.generator = generator;
        this.logger = logger;
    }


    @Override
    public void run(String... args) throws Exception {

        logger.info("Начата загрузка видов подписок в базу данных");


        if (!subscriptionTypeRepository.existsByDuration(SubscriptionDurationAndPriceConstants.ONE_WEEK.getDays())) {

            SubscriptionType subscriptionType = getSubscriptionType(SubscriptionDurationAndPriceConstants.ONE_WEEK);
            subscriptionTypeRepository.save(subscriptionType);
        }
        if (!subscriptionTypeRepository.existsByDuration(SubscriptionDurationAndPriceConstants.TWO_WEEKS.getDays())) {

            SubscriptionType subscriptionType = getSubscriptionType(SubscriptionDurationAndPriceConstants.TWO_WEEKS);
            subscriptionTypeRepository.save(subscriptionType);
        }
        if (!subscriptionTypeRepository.existsByDuration(SubscriptionDurationAndPriceConstants.ONE_MONTH.getDays())) {

            SubscriptionType subscriptionType = getSubscriptionType(SubscriptionDurationAndPriceConstants.ONE_MONTH);
            subscriptionTypeRepository.save(subscriptionType);
        }
        if (!subscriptionTypeRepository.existsByDuration(SubscriptionDurationAndPriceConstants.ONE_YEAR.getDays())) {

            SubscriptionType subscriptionType = getSubscriptionType(SubscriptionDurationAndPriceConstants.ONE_YEAR);
            subscriptionTypeRepository.save(subscriptionType);
        }

        logger.info("Завершена загрузка видов подписок в базу данных");
    }

    private SubscriptionType getSubscriptionType(SubscriptionDurationAndPriceConstants period) {
        SubscriptionType subscriptionType = SubscriptionType.builder()
                .duration(period.getDays())
                .price(period.getPrice())
                .build();

        subscriptionType.setId(generator);
        return subscriptionType;
    }
}