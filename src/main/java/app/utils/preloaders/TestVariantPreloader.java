package app.utils.preloaders;

import app.deserializer.QuestionDeserializer;
import app.models.Question;
import app.models.Subject;
import app.models.TestType;
import app.models.TestVariant;
import app.repositories.QuestionRepository;
import app.repositories.SubjectRepository;
import app.repositories.TestTypeRepository;
import app.repositories.TestVariantRepository;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Component
@Order(6)
public class TestVariantPreloader implements CommandLineRunner {

    private final TestVariantRepository testVariantRepository;
    private final SubjectRepository subjectRepository;
    private final TestTypeRepository testTypeRepository;
    private final QuestionRepository questionRepository;
    private final UUIDGenerator<Question> questionUUIDGenerator;
    private final UUIDGenerator<TestVariant> testUUIDGenerator;
    private final Logger logger;

    public TestVariantPreloader(TestVariantRepository testVariantRepository, SubjectRepository subjectRepository, TestTypeRepository testTypeRepository, QuestionRepository questionRepository, UUIDGenerator<Question> questionUUIDGenerator, UUIDGenerator<TestVariant> testUUIDGenerator, Logger logger) {
        this.testVariantRepository = testVariantRepository;
        this.subjectRepository = subjectRepository;
        this.testTypeRepository = testTypeRepository;
        this.questionRepository = questionRepository;
        this.questionUUIDGenerator = questionUUIDGenerator;
        this.testUUIDGenerator = testUUIDGenerator;
        this.logger = logger;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        if (!testVariantRepository.existsByDescription("ЕНТ пробный тест")){
            TestVariant testVariant = new TestVariant();
            testVariant.setId(testUUIDGenerator);

            TestType testType = new TestType();

            if (testTypeRepository.existsByTitle("ЕНТ")){
                testType = testTypeRepository.findTestTypeByTitle("ЕНТ");
                testVariant.setTestTypeId(testType.getId());
            }
            else {
                throw new NoSuchElementException("During preloading testVariant testType \"ЕНТ\" wasn't found");
            }

            List<Subject> subjectsByTestType = subjectRepository.findAllByTestType(testType).orElseThrow(() ->
                    new NoSuchElementException("There aren't subjects by this testType")
            );

            List<Subject> testSubjects = new ArrayList<>();

            for (Subject subject : subjectsByTestType) {
                if (subject.isRequired()) {
                    testSubjects.add(subject);
                }
            }

            testVariant.setSubjectList(testSubjects);
            testVariant.setDescription("ЕНТ пробный тест");
            testVariant.setLanguage("ru");

            List<Question> questions = QuestionDeserializer.loadQuestions("src/main/resources/data/ENT_SAMPLE.json");

            TestType finalTestType = testType;
            questions.forEach(el -> {
                String title = el.getSubjectId();
                Subject subject = subjectRepository.findSubjectByTitleAndTestType(title, finalTestType);
                el.setSubjectId(subject.getId());
            });
            questions.forEach(el -> el.setId(questionUUIDGenerator));
            questions.forEach(el -> el.setTestId(testVariant.getId()));

            testVariantRepository.save(testVariant);
            questionRepository.saveAll(questions);
        }

        if (!testVariantRepository.existsByDescription("ЕНТ Вариант 1")){
            TestVariant testVariantEntVar1 = new TestVariant();
            testVariantEntVar1.setId(testUUIDGenerator);

            TestType testType = new TestType();

            if (testTypeRepository.existsByTitle("ЕНТ")){
                testType = testTypeRepository.findTestTypeByTitle("ЕНТ");
                testVariantEntVar1.setTestTypeId(testType.getId());
            }
            else {
                logger.error("При предзагрузки \"ЕНТ Вариант 1\" в базе не был найден вид теста \"ЕНТ\"");
                throw new NoSuchElementException("During preloading testVariant testType \"ЕНТ\" wasn't found");
            }

            TestType finalTestType = testType;
            List<Subject> subjectsByTestType = subjectRepository.findAllByTestType(testType).orElseThrow(() -> {
                    logger.error("При предзагрузки \"ЕНТ Вариант 1\" в базе не было найдено предметов по виду теста:{} ", finalTestType);
                    throw new NoSuchElementException("There aren't subjects by this testType");
            });

            testVariantEntVar1.setSubjectList(subjectsByTestType);

            testVariantEntVar1.setDescription("ЕНТ Вариант 1");
            testVariantEntVar1.setLanguage("ru");

            List<Question> questions = QuestionDeserializer.loadQuestions("src/main/resources/data/ENT_RU_VAR_1.json");

            createQuestions(testType, questions, testVariantEntVar1);

            testVariantRepository.save(testVariantEntVar1);
            questionRepository.saveAll(questions);
        }
        logger.info("ЕНТ Вариант 1 загружен в базу");

        if (!testVariantRepository.existsByDescription("ЕНТ Вариант 2")){
            TestVariant testVariantEntVar2 = new TestVariant();
            testVariantEntVar2.setId(testUUIDGenerator);

            TestType testType = new TestType();

            if (testTypeRepository.existsByTitle("ЕНТ")){
                testType = testTypeRepository.findTestTypeByTitle("ЕНТ");
                testVariantEntVar2.setTestTypeId(testType.getId());
            }
            else {
                logger.error("При предзагрузки \"ЕНТ Вариант 2\" в базе не был найден вид теста \"ЕНТ\"");
                throw new NoSuchElementException("During preloading testVariant testType \"ЕНТ\" wasn't found");
            }

            TestType finalTestType = testType;
            List<Subject> subjectsByTestType = subjectRepository.findAllByTestType(testType).orElseThrow(() -> {
                logger.error("При предзагрузки \"ЕНТ Вариант 1\" в базе не было найдено предметов по виду теста:{} ", finalTestType);
                throw new NoSuchElementException("There aren't subjects by this testType");
            });

            testVariantEntVar2.setSubjectList(subjectsByTestType);

            testVariantEntVar2.setDescription("ЕНТ Вариант 2");
            testVariantEntVar2.setLanguage("ru");

            List<Question> questions = QuestionDeserializer.loadQuestions("src/main/resources/data/ENT_RU_VAR_1.json");

            createQuestions(testType, questions, testVariantEntVar2);

            testVariantRepository.save(testVariantEntVar2);
            questionRepository.saveAll(questions);
        }

        if (!testVariantRepository.existsByDescription("ОЗП Вариант 1")){
            TestVariant testVariantOzpVar1 = new TestVariant();
            testVariantOzpVar1.setId(testUUIDGenerator);

            TestType testType = new TestType();

            if (testTypeRepository.existsByTitle("ОЗП")){
                testType = testTypeRepository.findTestTypeByTitle("ОЗП");
                testVariantOzpVar1.setTestTypeId(testType.getId());
            }
            else {
                throw new NoSuchElementException("During preloading testVariant testType \"ОЗП\" wasn't found");
            }
            TestType finalTestType = testType;
            List<Subject> subjectsByTestType = subjectRepository.findAllByTestType(testType).orElseThrow(() -> {
                logger.error("При предзагрузки \"ОЗП Вариант 1\" в базе не было найдено предметов по виду теста:{} ", finalTestType);
                throw new NoSuchElementException("There aren't subjects by this testType");
            });

            testVariantOzpVar1.setSubjectList(subjectsByTestType);
            testVariantOzpVar1.setDescription("ОЗП Вариант 1");
            testVariantOzpVar1.setLanguage("ru");

            List<Question> questions = QuestionDeserializer.loadQuestions("src/main/resources/data/OZP_RU_VAR_1.json");

            createQuestions(testType, questions, testVariantOzpVar1);

            testVariantRepository.save(testVariantOzpVar1);
            questionRepository.saveAll(questions);

            logger.info("ОЗП Вариант 1 загружен в базу");
        }
    }

    private void createQuestions(TestType testType, List<Question> questions, TestVariant testVariant) {
        questions.forEach(el -> {
            String title = el.getSubjectId();
            Subject subject = subjectRepository.findSubjectByTitleAndTestType(title, testType);
            if (subject == null){
                logger.error("При загрузке теста \"{}\" произошла ошибка, в базе не был найден предмет с именем \"{}\"",
                        testVariant.getDescription(), title);
            }
            if (subject != null) {
                el.setSubjectId(subject.getId());
            }
        });
        questions.forEach(el -> el.setId(questionUUIDGenerator));
        questions.forEach(el -> el.setTestId(testVariant.getId()));
    }
}