package app.utils.preloaders;

import app.models.Subject;
import app.models.TestType;
import app.repositories.SubjectRepository;
import app.repositories.TestTypeRepository;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Order(5)
public class SubjectPreloader implements CommandLineRunner {

    private final SubjectRepository subjectRepository;
    private final TestTypeRepository testTypeRepository;
    private final UUIDGenerator generator;
    private final Logger logger;
    public SubjectPreloader(SubjectRepository subjectRepository, TestTypeRepository testTypeRepository, UUIDGenerator generator, Logger logger) {
        this.subjectRepository = subjectRepository;
        this.testTypeRepository = testTypeRepository;
        this.generator = generator;
        this.logger = logger;
    }


    @Override
    public void run(String... args) throws Exception {
        logger.info("Начата загрузка предметов в базу данных");
        if (!subjectRepository.existsByTitleAndTestType("Математическая грамотность", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Математическая грамотность");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Грамотность чтения", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Грамотность чтения");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("История Казахстана", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("История Казахстана");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Математика", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Математика");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Физика", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Физика");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Информатика", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Информатика");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("География", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("География");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Биология", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Биология");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Химия", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Химия");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Иностранный язык", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Иностранный язык");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Всемирная история", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Всемирная история");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Основы права", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Основы права");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Казахский язык", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Казахский язык");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Русский язык", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Русский язык");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Казахская литература", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Казахская литература");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Русская литература", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Русская литература");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Творческий", getTestType("ЕНТ"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Творческий");
            subject.setTestType(getTestType("ЕНТ"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Математика", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Математика");
            subject.getDependencies().add(getDependentSubjectId("Физика"));
            subject.getDependencies().add(getDependentSubjectId("Информатика"));
            subject.getDependencies().add(getDependentSubjectId("География"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Физика", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Физика");
            subject.getDependencies().add(getDependentSubjectId("Математика"));
            subject.getDependencies().add(getDependentSubjectId("Химия"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Информатика", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Информатика");
            subject.getDependencies().add(getDependentSubjectId("Математика"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("География", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("География");
            subject.getDependencies().add(getDependentSubjectId("Математика"));
            subject.getDependencies().add(getDependentSubjectId("Биология"));
            subject.getDependencies().add(getDependentSubjectId("Иностранный язык"));
            subject.getDependencies().add(getDependentSubjectId("Всемирная история"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Биология", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Биология");
            subject.getDependencies().add(getDependentSubjectId("Химия"));
            subject.getDependencies().add(getDependentSubjectId("География"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Химия", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Химия");
            subject.getDependencies().add(getDependentSubjectId("Биология"));
            subject.getDependencies().add(getDependentSubjectId("Физика"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Иностранный язык", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Иностранный язык");
            subject.getDependencies().add(getDependentSubjectId("География"));
            subject.getDependencies().add(getDependentSubjectId("Всемирная история"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Всемирная история", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Всемирная история");
            subject.getDependencies().add(getDependentSubjectId("География"));
            subject.getDependencies().add(getDependentSubjectId("Иностранный язык"));
            subject.getDependencies().add(getDependentSubjectId("Основы права"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Основы права", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Основы права");
            subject.getDependencies().add(getDependentSubjectId("Всемирная история"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Казахский язык", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Казахский язык");
            subject.getDependencies().add(getDependentSubjectId("Казахская литература"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Казахская литература", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Казахская литература");
            subject.getDependencies().add(getDependentSubjectId("Казахский язык"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Русский язык", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Русский язык");
            subject.getDependencies().add(getDependentSubjectId("Русская литература"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Русская литература", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Русская литература");
            subject.getDependencies().add(getDependentSubjectId("Русский язык"));
            subjectRepository.save(subject);
        }

        if (isDependenciesEmpty("Творческий", "ЕНТ")) {
            Subject subject = subjectRepository.findSubjectByTitle("Творческий");
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitle("Reading")) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Reading");
            subject.setTestType(getTestType("TOEFL"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitle("Listening")) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Listening");
            subject.setTestType(getTestType("TOEFL"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Методика преподавания", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Методика преподавания");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(true);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Начальные классы", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Начальные классы");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Русская литература", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Русская литература");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Русский язык", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Русский язык");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Физика", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Физика");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Химия", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Химия");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Биология", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Биология");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("География", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("География");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Информатика", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Информатика");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("История Казахстана", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("История Казахстана");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Всемирная история", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Всемирная история");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Математика", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Математика");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Музыка", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Музыка");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        if (!subjectRepository.existsByTitleAndTestType("Черчение", getTestType("ОЗП"))) {
            Subject subject = new Subject();
            subject.setId(generator);
            subject.setTitle("Черчение");
            subject.setTestType(getTestType("ОЗП"));
            subject.setDependencies(new ArrayList<>());
            subject.setRequired(false);
            subjectRepository.save(subject);
        }

        logger.info("Закончена загрузка предметов в базу данных");
    }

    public boolean isDependenciesEmpty(String title, String testType) {
        return subjectRepository.findSubjectByTitleAndTestType(title, getTestType(testType)).getDependencies().isEmpty();
    }

    public TestType getTestType(String title) {
        return testTypeRepository.findTestTypeByTitle(title);
    }

    public String getDependentSubjectId(String title) {
        return subjectRepository.findSubjectByTitle(title).getId();
    }
}
