package app.utils.preloaders;

import app.configuration.constants.EmployeesAmountConstants;
import app.models.EmployeeAmount;
import app.repositories.*;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class EmployeeAmountPreloader implements CommandLineRunner {

    private final Logger logger;
    private final EmployeeAmountRepository employeeAmountRepository;
    private final UUIDGenerator generator;

    public EmployeeAmountPreloader(Logger logger, EmployeeAmountRepository employeeAmountRepository, UUIDGenerator generator) {
        this.logger = logger;
        this.employeeAmountRepository = employeeAmountRepository;
        this.generator = generator;
    }


    public void run(String... args) throws Exception {

        if (!employeeAmountRepository.existsByEmployeeAmount(EmployeesAmountConstants.FIFTY.getEmployeesAmount())){
            EmployeeAmount build = getEmployeeAmountForCompaniesSubscription(EmployeesAmountConstants.FIFTY);

            employeeAmountRepository.save(build);
            logger.info("В базу загружены константы сотрудников (50 человек) для компаний и скидка");
        }
        if (!employeeAmountRepository.existsByEmployeeAmount(EmployeesAmountConstants.HUNDRED.getEmployeesAmount())){
            EmployeeAmount build = getEmployeeAmountForCompaniesSubscription(EmployeesAmountConstants.HUNDRED);

            employeeAmountRepository.save(build);
            logger.info("В базу загружены константы сотрудников (100 человек) для компаний и скидка");


        }
        if (!employeeAmountRepository.existsByEmployeeAmount(EmployeesAmountConstants.MORE_THAN_HUNDRED.getEmployeesAmount())){
            EmployeeAmount build = getEmployeeAmountForCompaniesSubscription(EmployeesAmountConstants.MORE_THAN_HUNDRED);

            employeeAmountRepository.save(build);
            logger.info("В базу загружены константы сотрудников (200 человек) для компаний и скидка");


        }
    }

    private EmployeeAmount getEmployeeAmountForCompaniesSubscription(EmployeesAmountConstants amount) {
        EmployeeAmount build = EmployeeAmount.builder()
                .employeeAmount(amount.getEmployeesAmount())
                .discount(amount.getDiscount())
                .build();

        String generated = generator.generate(build);
        build.setId(generated);
        return build;
    }
}