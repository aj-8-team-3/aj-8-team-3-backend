package app.utils;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDGenerator<TEntity> {
    public String generate(TEntity tEntity) {
        String id = UUID.randomUUID().toString();

        return switch (tEntity.getClass().getSimpleName()) {
            case "User" -> id + "_USER";
            case "Authority" -> id + "_AUTHORITY";
            case "UserAuthority" -> id + "_USER_AUTHORITY";
            case "Place" -> id + "_PLACE";
            case "Image" -> id + "_IMAGE";
            case "Gallery" -> id + "_GALLERY";
            case "SubscriptionType" -> id + "_SUBSCRIPTION_TYPE";
            case "EmployeeAmount" -> id + "_EMPLOYEE_AMOUNT";
            case "Company" -> id + "_COMPANY";
            case "Subscription" -> id + "_SUBSCRIPTION";
            case "UserSubscription" -> id + "_USER_SUBSCRIPTION";
            case "TestType" -> id + "_TEST_TYPE";
            case "Subject" -> id + "_SUBJECT";
            case "TestVariant" -> id + "_TEST_VARIANT";
            case "Question" -> id + "_QUESTION";
            case "HistoryTest" -> id + "_HISTORY_TEST";
            case "HistoryQuestion" -> id + "_HISTORY_QUESTION";
            case "Addition" -> id + "_ADDITION";
            default -> throw new IllegalArgumentException();
        };
    }
}
