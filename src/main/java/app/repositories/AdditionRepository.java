package app.repositories;

import app.models.Addition;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AdditionRepository extends MongoRepository<Addition, String> {
    List<Addition> findAllByTestId(String testId);
}
