package app.repositories;

import app.models.UserAuthority;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthorityRepository extends MongoRepository<UserAuthority, String> {
    UserAuthority findByUserId(String userId);

    UserAuthority getUserAuthoritiesByUserId(String userId);
}
