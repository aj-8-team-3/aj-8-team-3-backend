package app.repositories;

import app.models.SubscriptionType;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubscriptionTypeRepository extends MongoRepository<SubscriptionType, String> {
    boolean existsByDuration(int duration);

    SubscriptionType findSubscriptionTypeByDuration(int duration);
}
