package app.repositories;

import app.models.Authority;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends MongoRepository<Authority, String> {
    Authority findByAuthority(String authority);

    boolean existsByAuthority(String authority);
}
