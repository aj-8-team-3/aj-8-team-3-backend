package app.repositories;

import app.models.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface QuestionRepository extends MongoRepository<Question, String> {
    List<Question> findAllByTestId(String testId);
    List<Question> findAllBySubjectId(String subjectId);
}
