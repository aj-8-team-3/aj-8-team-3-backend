package app.repositories;

import app.models.TestVariant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestVariantRepository extends MongoRepository<TestVariant, String> {

    TestVariant findTestVariantById(String id);
    List<TestVariant> findAllByTestTypeIdAndLanguage(String id, String language);
    TestVariant findTestVariantByDescription(String description);
    boolean existsByDescription(String description);
}
