package app.repositories;

import app.models.TestType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestTypeRepository extends MongoRepository<TestType, String> {

    TestType findTestTypeByTitle(String title);

    boolean existsByTitle (String title);
}
