package app.repositories;

import app.models.Subject;
import app.models.TestType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends MongoRepository<Subject, String> {

    Optional<List<Subject>> findAllByTestType(TestType testType);
    Subject findSubjectByTitle(String title);
    Subject findSubjectByTitleAndTestType(String title, TestType testType);
    boolean existsByTitle(String title);
    boolean existsByTitleAndTestType(String title, TestType testType);
}
