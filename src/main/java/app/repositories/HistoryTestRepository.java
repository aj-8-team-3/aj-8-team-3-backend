package app.repositories;

import app.models.HistoryTest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryTestRepository extends MongoRepository<HistoryTest, String> {
    List<HistoryTest> findAllByUserId(String userId);
}
