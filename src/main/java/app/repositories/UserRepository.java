package app.repositories;

import app.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Boolean existsByIdentifier(String phoneNumber);

    User findByIdentifier(String phoneNumber);

}
