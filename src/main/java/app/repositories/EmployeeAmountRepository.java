package app.repositories;

import app.models.EmployeeAmount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeAmountRepository extends MongoRepository<EmployeeAmount, String> {
    boolean existsByEmployeeAmount(int employeeAmount);

    EmployeeAmount findEmployeeAmountByEmployeeAmount(int employeeAmount);
}
