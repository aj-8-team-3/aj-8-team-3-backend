package app.repositories;

import app.models.UserSubscription;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSubscriptionRepository extends MongoRepository<UserSubscription, String> {

    UserSubscription findByUserId(String userId);

}
