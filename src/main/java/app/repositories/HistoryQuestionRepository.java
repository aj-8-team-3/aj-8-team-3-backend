package app.repositories;

import app.models.HistoryQuestion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryQuestionRepository extends MongoRepository<HistoryQuestion, String> {
}
