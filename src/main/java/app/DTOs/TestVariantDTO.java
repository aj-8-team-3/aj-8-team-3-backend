package app.DTOs;

import app.models.Subject;
import app.models.TestVariant;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestVariantDTO {
    private String id;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String testTypeId;

    @NotEmpty
    @Size(min = 1, max = 255)
    private String description;

    @NotNull
    private List<Subject> subjectList;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String language;

    public TestVariant toTestVariant() {
        TestVariant testVariant = new TestVariant();
        testVariant.setTestTypeId(this.testTypeId);
        testVariant.setDescription(this.description);
        testVariant.setSubjectList(this.subjectList);
        testVariant.setLanguage(this.language);
        return testVariant;
    }

    public static TestVariantDTO fromTestVariant(TestVariant testVariant) {
        return new TestVariantDTO(testVariant.getId(), testVariant.getTestTypeId(), testVariant.getDescription(), testVariant.getSubjectList(), testVariant.getLanguage());
    }
}