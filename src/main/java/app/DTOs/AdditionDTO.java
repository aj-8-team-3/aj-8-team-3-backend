package app.DTOs;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.NotNull;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdditionDTO {

    private String id;

    private String testId;

    @NotBlank(message = "The question must belong to some subject")
    private String subjectId;

    @NotBlank(message = "Content type is required!")
    @Pattern(regexp = "VIDEO|IMAGE|AUDIO|TEXT", message = "Invalid content type!")
    private String type;

    private String contentText = "";

    private String contentFile;
}
