package app.DTOs;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTestVariantDTO {

    @NotEmpty (message = "You have to set TestTypeId")
    private String testTypeId;

    @NotEmpty (message = "Description can't be empty")
    private String description;

    @NotEmpty (message = "You have to choose at least one subject")
    private List<String> subjectList = new ArrayList<>();

    @NotEmpty (message = "Choose language please")
    @Pattern(regexp = "ru|kz|en", message = "Invalid language!")
    private String language;

    @NotEmpty (message = "You can't create test without questions")
    private List<QuestionDTO> questions = new ArrayList<>();

    private List<AdditionDTO> additions = new ArrayList<>();
}