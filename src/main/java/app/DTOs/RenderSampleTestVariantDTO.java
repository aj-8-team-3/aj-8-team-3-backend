package app.DTOs;


import app.models.Addition;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RenderSampleTestVariantDTO {

    private String testId;
    private String description;
    private List<String> subjectList = new ArrayList<>();
    private List<RenderQuestionDTO> questions = new ArrayList<>();
    private List<Addition> additions = new ArrayList<>();
}