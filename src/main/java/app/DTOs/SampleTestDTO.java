package app.DTOs;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.List;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class SampleTestDTO {

    @NotEmpty
    List<HistoryQuestionDTO> questions;
}
