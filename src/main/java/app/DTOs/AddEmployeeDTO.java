package app.DTOs;


import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddEmployeeDTO {
    private String companyId;
    private String employeeId;
    private List<String> subIds;
}
