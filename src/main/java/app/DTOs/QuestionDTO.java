package app.DTOs;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {

    private String id;

    private String testId;

    @NotBlank(message = "the question must belong to some subject")
    private String subjectId;

    @NotBlank(message = "title is required")
    private String title;

    @NotBlank(message = "Answers is required")
    private List<String> answers;

    @NotBlank(message = "Right answer is required")
    private List<String> rightAnswers;
}