package app.DTOs;


import app.models.Addition;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RenderTestVariantDTO {
    private String testId;
    private String testTypeId;
    private String description;
    private List<String> subjectsList = new ArrayList<>();
    private String language;
    private List<RenderQuestionDTO> questions = new ArrayList<>();
    private List<Addition> additions = new ArrayList<>();
}