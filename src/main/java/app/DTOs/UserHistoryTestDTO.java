package app.DTOs;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserHistoryTestDTO {

    private String title;

    private List<String> subjects;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime submissionDate;

    private Integer result;
}
