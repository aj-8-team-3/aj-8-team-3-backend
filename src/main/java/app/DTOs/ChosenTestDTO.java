package app.DTOs;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChosenTestDTO {
    private String lang;
    private String testTypeId;
    private List<String> subjectsIds;
}
