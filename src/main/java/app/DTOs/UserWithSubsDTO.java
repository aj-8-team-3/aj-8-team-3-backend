package app.DTOs;

import app.models.HistoryTest;
import app.models.Subscription;
import lombok.*;

import java.util.List;

@Builder
@Getter@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserWithSubsDTO {
    private String id;
    private String email;
    private List<Subscription> subs;
    private List<UserHistoryTestDTO> passedTests;
}
