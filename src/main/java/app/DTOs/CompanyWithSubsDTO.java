package app.DTOs;

import app.models.Subscription;
import app.models.User;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyWithSubsDTO {
    private String id;
    private String bin;
    private List<Subscription> subs;
    private List<User> employees;
}
