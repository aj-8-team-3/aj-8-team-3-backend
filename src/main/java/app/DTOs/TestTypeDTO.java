package app.DTOs;

import app.models.TestType;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestTypeDTO {

    private String id;
    private String title;
    private Integer subscriptionPrice;
    private Integer instantPrice;
    private Boolean isMultiLang;

    public static TestTypeDTO fromTestType(TestType testType) {
        return new TestTypeDTO(testType.getId(), testType.getTitle(), testType.getSubscriptionPrice(), testType.getInstantPrice(), testType.getIsMultiLang());
    }
}
