package app.DTOs;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.List;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryTestDTO {

    @NotEmpty
    private String testId;

    @NotEmpty
    private List<String> subjectsList;

    @NotEmpty
    private List<HistoryQuestionDTO> questions;
}
