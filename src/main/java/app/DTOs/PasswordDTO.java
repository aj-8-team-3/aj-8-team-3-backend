package app.DTOs;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PasswordDTO {
    String token;
    @NotBlank(message = "Password is required")
    @Size(min = 8, message = "password must not be shorter than 8 symbols")
    private String password;
}
