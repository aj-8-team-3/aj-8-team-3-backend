package app.DTOs;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionDTO {

    @NotEmpty(message = "Вы не выбрали тест!")
    private String[] testTypes;

    @NotNull
    private int subscriptionDuration;

    private int employeesAmount;

    @NotNull
    private int cost;
}
