package app.DTOs;

import lombok.*;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectDTO {

    private String title;

    private String testType;

    private String[] dependencies;

    private String isRequired;
}
