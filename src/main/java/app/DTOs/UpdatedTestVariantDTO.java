package app.DTOs;

import app.models.Question;
import app.models.Subject;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdatedTestVariantDTO {
    @NotEmpty
    @Size(min = 1, max = 255)
    private String description;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String language;

    @NotNull
    private List<Subject> subjectList;

    @NotNull
    private List<Question> questions;
}
