package app.DTOs;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryQuestionDTO {

    @NotEmpty
    String questionId;

    @NotEmpty
    List<String> chosenAnswers;
}
