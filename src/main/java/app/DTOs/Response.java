package app.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter   @Setter
@AllArgsConstructor
public class Response<T> {
    private String message;
    private T response;
    private Integer status;
    private Boolean success;
}
