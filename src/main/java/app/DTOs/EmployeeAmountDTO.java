package app.DTOs;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeAmountDTO {
    private String id;
    private int employeeAmount;
    private int discount;
}
