package app.DTOs;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class RenderQuestionDTO {
    private String id;
    private String testId;
    private String subjectId;
    private String title;
    private List<String> answers;
}