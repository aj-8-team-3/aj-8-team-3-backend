package app.DTOs;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionTypeDTO {

    private  String id;

    @NotNull(message = "You have to specify duration")
    @Min(7)
    private int  duration;

    @NotNull (message = "You have to specify price")
    @Min(1000)
    private int price;
}
