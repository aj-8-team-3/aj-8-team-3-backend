package app.configuration.constants;

import lombok.Getter;

@Getter
public enum EmployeesAmountConstants {
    FIFTY (50, 5),
    HUNDRED (100, 10),
    MORE_THAN_HUNDRED (200, 15);
    private final int employeesAmount;
    private final int discount;

    EmployeesAmountConstants(int employeesAmount, int discount){
        this.employeesAmount = employeesAmount;
        this.discount = discount;
    }
}
