package app.configuration.constants;

import lombok.Getter;

@Getter
public enum SubscriptionDurationAndPriceConstants {
    ONE_WEEK (7, 5000),
    TWO_WEEKS (14, 10000),
    ONE_MONTH (30, 20000),
    ONE_YEAR (365, 100000);

    private final int days;
    private final int price;

    SubscriptionDurationAndPriceConstants(int days, int price){
        this.days = days;
        this.price = price;
    }
}
