package app.configuration;

import app.exceptions.SessionTokenExpiredException;
import app.service.TokenService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@AllArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    private app.configuration.tokenProviders.SessionJwtTokenProvider sessionJwtTokenProvider;
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {

        String path = request.getRequestURI();

        if ("/auth/authorize".equals(path)
                || "/verification".equals(path)
                || "/auth/getPasswordResetToken".equals(path)
                || "/auth/resetPassword".equals(path)
                || "/auth/savePassword".equals(path)
                || "/auth/getMailValidationToken".equals(path)
                || "/test-types/all".equals(path)
                || "/subscription_types".equals(path)
                || "/subjects".equals(path)
                || path.contains("/sample-test")) {
            filterChain.doFilter(request, response);
            return;
        }

        String accessToken = sessionJwtTokenProvider.resolveToken(request, "accessToken");
        String refreshToken = sessionJwtTokenProvider.resolveToken(request, "refreshToken");


        try {
            if (accessToken != null
                    && sessionJwtTokenProvider.validateToken(accessToken)) {

                Authentication auth = sessionJwtTokenProvider.getAuthentication(accessToken);
                SecurityContextHolder.getContext().setAuthentication(auth);
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token has been revoked");
                return;
            }
        } catch (SessionTokenExpiredException e) {
            try {
                if (refreshToken != null
                        && sessionJwtTokenProvider.validateToken(refreshToken)) {
                    Authentication auth = sessionJwtTokenProvider.getAuthentication(refreshToken);
                    SecurityContextHolder.getContext().setAuthentication(auth);

                    deleteCookie(request, response, "accessToken");
                    deleteCookie(request, response, "refreshToken");

                    String newAccessToken = sessionJwtTokenProvider.createSessionTokens(auth, request.getHeader("User-Agent"), true);
                    String newRefreshToken = sessionJwtTokenProvider.createSessionTokens(auth, request.getHeader("User-Agent"), false);

                    String accessCookieHeader = String.format(
                            "accessToken=%s; Path=/; Max-Age=172800; SameSite=Strict",
                            newAccessToken
                    );
                    response.addHeader("Set-Cookie", accessCookieHeader);

                    String refreshCookieHeader = String.format(
                            "refreshToken=%s; Path=/; Max-Age=172800; SameSite=Strict",
                            newRefreshToken
                    );
                    response.addHeader("Set-Cookie", refreshCookieHeader);
                }
                else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token has been revoked");
                    return;
                }
            } catch (SessionTokenExpiredException ex) {
                deleteCookie(request, response, "accessToken");
                deleteCookie(request, response, "refreshToken");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write("Token expired.");
                return;
            }
        }

        filterChain.doFilter(request, response);
    }

    private void deleteCookie(HttpServletRequest request, HttpServletResponse response, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    cookie.setValue("");
                    cookie.setPath("/");
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
    }
}
