package app.configuration;

import com.sun.security.auth.UserPrincipal;
import org.apache.logging.log4j.Logger;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;

public class UserHandshakeHandler extends DefaultHandshakeHandler {
    private final Logger logger;

    public UserHandshakeHandler(Logger logger) {
        this.logger = logger;
    }

    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        String name = request.getPrincipal().getName();
        if (name != null){
            logger.info("Пользователь: {} подключился к вебсокету", name);
        }
         return new UserPrincipal(name);
    }

}
