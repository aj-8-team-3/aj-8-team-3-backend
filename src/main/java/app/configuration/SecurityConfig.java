package app.configuration;

import app.exceptionHandlers.CustomAccessDeniedHandler;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@AllArgsConstructor
public class SecurityConfig {
    private final CustomAccessDeniedHandler customAccessDeniedHandler;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenFilter jwtTokenFilter;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .exceptionHandling(
                        exceptionHandling -> exceptionHandling
                                .accessDeniedHandler(customAccessDeniedHandler)
                )
                .authorizeHttpRequests(
                        authorize -> authorize
                                .requestMatchers("/logs/**").permitAll()
                                .requestMatchers("/verification").permitAll()
                                .requestMatchers("/auth/getPasswordResetToken").permitAll()
                                .requestMatchers("/auth/resetPassword").permitAll()
                                .requestMatchers("/auth/savePassword").permitAll()
                                .requestMatchers("/auth/getMailValidationToken").anonymous()
                                .requestMatchers("/auth/authorize").anonymous()
                                .requestMatchers("/auth/logout").authenticated()
                                .requestMatchers("/auth/check_device").authenticated()
                                .requestMatchers("/auth/check-access").authenticated()
                                .requestMatchers("/auth/isVerified").authenticated()
                                .requestMatchers("/test-types/all").permitAll()
                                .requestMatchers("/subscription_types").permitAll()
                                .requestMatchers("/subjects").permitAll()
                                .requestMatchers("/sample-test/**").permitAll()
                                .anyRequest().authenticated()
                )
                .csrf(AbstractHttpConfigurer::disable)
                .cors(
                        cors -> cors
                                .configurationSource(corsConfigurationSource())
                )
                .sessionManagement(
                        sessions -> sessions
                                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .formLogin(Customizer.withDefaults())
                .httpBasic(Customizer.withDefaults());

        return http.build();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.setAllowedHeaders(List.of("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH"));
        configuration.addAllowedOrigin("http://localhost:8080");

        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        AuthenticationManagerBuilder authManagerBuilder = new AuthenticationManagerBuilder(objectPostProcessor());
        authManagerBuilder.authenticationProvider(authenticationProvider());
        return authManagerBuilder.build();
    }

    private ObjectPostProcessor<Object> objectPostProcessor() {
        return new ObjectPostProcessor<Object>() {
            @Override
            public <T> T postProcess(T object) {
                return object;
            }
        };
    }
}
