package app.configuration.tokenProviders;

import app.exceptions.SessionTokenExpiredException;
import app.service.TokenService;
import app.service.impl.CustomUserDetailsServiceImpl;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SessionJwtTokenProvider {
    private final CustomUserDetailsServiceImpl customUserDetailsService;
    private final String JWT_SECRET;
    private final long accessValidityInMilliseconds;
    private final long refreshValidityInMilliseconds;
    private final TokenService tokenService;

    @Autowired
    public SessionJwtTokenProvider(CustomUserDetailsServiceImpl customUserDetailsService,
                            TokenService tokenService,
                            @Value("${jwt.secret}") String JWT_SECRET,
                            @Value("${session.access.token.expiry.time}") long accessValidityInMilliseconds,
                            @Value("${session.refresh.token.expiry.time}") long refreshValidityInMilliseconds) {
        this.customUserDetailsService = customUserDetailsService;
        this.tokenService = tokenService;
        this.JWT_SECRET = JWT_SECRET;
        this.accessValidityInMilliseconds = accessValidityInMilliseconds;
        this.refreshValidityInMilliseconds = refreshValidityInMilliseconds;
    }

    public String createSessionTokens(Authentication authentication, String device, boolean isAccess) {
        String phoneNumber = authentication.getName();
        List<String> roles = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        Date now = new Date();
        Date validity;

        if (isAccess) {
            validity = new Date(now.getTime() + accessValidityInMilliseconds);
        }
        else {
            validity = new Date(now.getTime() + refreshValidityInMilliseconds);
        }
        String token = JWT.create()
                .withSubject(phoneNumber)
                .withClaim("roles", roles)
                .withClaim("device", device)
                .withExpiresAt(validity)
                .sign(Algorithm.HMAC256(JWT_SECRET));

        if (isAccess) {
            tokenService.storeSessionAccessTokens(phoneNumber, token);
        }
        else {
            tokenService.storeSessionRefreshTokens(phoneNumber, token);
        }

        return token;
    }

    public String getSessionAccessTokens(String phoneNumber) {
        return tokenService.getSessionAccessToken(phoneNumber);
    }

    public String getIdentifierFromSessionToken(String token) {
        String subject;
        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);
            DecodedJWT jwt = JWT.require(algorithm)
                    .build()
                    .verify(token);

            subject = jwt.getSubject();
        } catch (TokenExpiredException e) {
            throw new TokenExpiredException("Token was expired");
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
        return subject;
    }

    public String getDeviceFromToken(String token) {
        String device;

        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);
            DecodedJWT jwt = JWT.require(algorithm)
                    .build()
                    .verify(token);

            device = jwt.getClaim("device").asString();
        } catch (TokenExpiredException e) {
            throw new TokenExpiredException("Token was expired");
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
        return device;
    }

    public String resolveToken(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        String authToken = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (key.equals(cookie.getName())) {
                    authToken = cookie.getValue();
                    break;
                }
            }
        }
        return authToken;
    }

    public boolean validateToken(String token) throws SessionTokenExpiredException {
        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);
            JWT.require(algorithm).build().verify(token);
            return true;
        } catch (TokenExpiredException e) {
            throw new SessionTokenExpiredException("Token was expired");
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(getIdentifierFromSessionToken(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }
}
