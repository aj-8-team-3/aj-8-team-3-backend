package app.configuration.tokenProviders;

import app.service.TokenService;
import app.service.impl.CustomUserDetailsServiceImpl;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MailValidationTokenProvider {
    private final CustomUserDetailsServiceImpl customUserDetailsService;
    private final String JWT_SECRET;
    private final long validityInMilliseconds;
    private final TokenService tokenService;

    @Autowired
    public MailValidationTokenProvider(CustomUserDetailsServiceImpl customUserDetailsService,
                                      TokenService tokenService,
                                      @Value("${jwt.secret}") String JWT_SECRET,
                                      @Value("${validation.token.expiry.time}") long validityInMilliseconds) {
        this.customUserDetailsService = customUserDetailsService;
        this.tokenService = tokenService;
        this.JWT_SECRET = JWT_SECRET;
        this.validityInMilliseconds = validityInMilliseconds;
    }

    public String createMailValidationToken(String email) {
        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        String token = JWT.create()
                .withSubject(email)
                .withExpiresAt(validity)
                .sign(Algorithm.HMAC256(JWT_SECRET));

        tokenService.storeMailValidationToken(email, token);

        return token;
    }

    public String getMailValidationToken(String email) {
        return tokenService.getMailValidationToken(email);
    }

    public String getIdentifierFromMailValidationToken(String token) throws TokenExpiredException, JWTVerificationException {
        String subject;
        if (token.startsWith("Bearer ")) {
            token = token.substring(7);
        }
        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);
            DecodedJWT jwt = JWT.require(algorithm)
                    .build()
                    .verify(token);

            subject = jwt.getSubject();
        } catch (TokenExpiredException e) {
            throw new TokenExpiredException("Token was expired");
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
        return subject;
    }

    public boolean validateToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);
            JWT.require(algorithm).build().verify(token);
            return true;
        } catch (TokenExpiredException e) {
            throw new TokenExpiredException("Token was expired");
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
    }
}
