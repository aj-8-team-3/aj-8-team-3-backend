package app.exceptionHandlers;

import app.DTOs.Response;
import app.exceptions.*;
import com.auth0.jwt.exceptions.TokenExpiredException;
import jakarta.mail.MessagingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<?> handleUserAlreadyExists(UserAlreadyExistsException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.CONFLICT.value(), false));
    }

    @ExceptionHandler(UserDoesntExistException.class)
    public ResponseEntity<?> handleUserDoesntExist(UserDoesntExistException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.CONFLICT.value(), false));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<?> handleIOException(IOException exception) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(new Response(exception.getMessage(), null, HttpStatus.CONFLICT.value(), false));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity.badRequest().body(errors);
    }
    @ExceptionHandler(MessagingException.class)
    public ResponseEntity<?> handleIOException(MessagingException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.CONFLICT.value(), false));
    }

    @ExceptionHandler(SubscriptionCostValidationException.class)
    public ResponseEntity<?> handleSubscriptionCostValidation(SubscriptionCostValidationException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.CONFLICT.value(), false));
    }

    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<?> handleTokenExpiredException(TokenExpiredException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.FORBIDDEN.value(), false));
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<?> handleNoSuchElementException(NoSuchElementException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.FORBIDDEN.value(), false));
    }

    @ExceptionHandler(NoSuchTestTypeException.class)
    public ResponseEntity<?> handleNoSuchTestTypeException(NoSuchTestTypeException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.FORBIDDEN.value(), false));
    }

    @ExceptionHandler(FileWasNotSavedException.class)
    public ResponseEntity<?> handleFileWasNottSavedException(FileWasNotSavedException exception) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(exception.getMessage(), null, HttpStatus.FORBIDDEN.value(), false));
    }
}
