package app.mappers;

import app.DTOs.TestVariantDTO;
import app.models.TestVariant;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TestVariantMapper {
    private final ModelMapper mapper;

    public TestVariantMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public TestVariantDTO mapToDto(TestVariant testVariant){
        return mapper.map(testVariant, TestVariantDTO.class);
    }
    public TestVariant mapToEntity(TestVariantDTO testVariantDTO){
        return mapper.map(testVariantDTO, TestVariant.class);
    }
}
