package app.mappers;
import app.DTOs.SubscriptionTypeDTO;
import app.models.SubscriptionType;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionTypeMapper {
    private final ModelMapper mapper;

    public SubscriptionTypeMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public SubscriptionTypeDTO mapToDto(SubscriptionType subscriptionType){
        return mapper.map(subscriptionType, SubscriptionTypeDTO.class);
    }

    public SubscriptionType mapToEntity(SubscriptionTypeDTO subscriptionTypeDTO){
        return mapper.map(subscriptionTypeDTO, SubscriptionType.class);
    }
}
