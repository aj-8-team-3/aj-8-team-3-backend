package app.mappers;

import app.DTOs.QuestionDTO;
import app.DTOs.RenderQuestionDTO;
import app.models.Question;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class QuestionMapper {
    private final ModelMapper mapper;

    public QuestionMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public QuestionDTO mapToDto(Question question){
        return mapper.map(question, QuestionDTO.class);
    }
    public Question mapToEntity(QuestionDTO questionDTO){
        return mapper.map(questionDTO, Question.class);
    }

    public RenderQuestionDTO mapToRenderDto(Question question){
        return mapper.map(question, RenderQuestionDTO.class);
    }

}
