package app.mappers;

import app.DTOs.TestTypeDTO;
import app.models.TestType;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TestTypeMapper {

    private final ModelMapper mapper;

    public TestTypeMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public TestTypeDTO mapToDto(TestType testType){
        return mapper.map(testType, TestTypeDTO.class);
    }

    public TestType mapToEntity(TestTypeDTO testTypeDTO){
        return mapper.map(testTypeDTO, TestType.class);
    }
}
