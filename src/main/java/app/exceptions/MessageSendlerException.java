package app.exceptions;

public class MessageSendlerException extends Exception {
    public MessageSendlerException(String message) {
        super(message);
    }
}
