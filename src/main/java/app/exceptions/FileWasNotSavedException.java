package app.exceptions;

public class FileWasNotSavedException extends Exception {
    public FileWasNotSavedException(String message) {
        super(message);
    }
}
