package app.exceptions;

public class NoSuchTestTypeException extends Exception {
    public NoSuchTestTypeException(String message) {
        super(message);
    }
}
