package app.exceptions;

public class SubscriptionErrorException extends Exception {
    public SubscriptionErrorException(String message) {
        super(message);
    }
}
