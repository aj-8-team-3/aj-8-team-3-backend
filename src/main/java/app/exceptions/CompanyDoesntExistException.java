package app.exceptions;

public class CompanyDoesntExistException extends Exception {
    public CompanyDoesntExistException(String message)  {
        super(message);
    }
}
