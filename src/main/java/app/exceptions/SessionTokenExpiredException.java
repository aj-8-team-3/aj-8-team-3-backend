package app.exceptions;

public class SessionTokenExpiredException extends Exception{
    public SessionTokenExpiredException(String message) {
        super(message);
    }
}
