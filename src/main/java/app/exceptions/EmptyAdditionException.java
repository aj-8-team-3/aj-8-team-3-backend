package app.exceptions;

public class EmptyAdditionException extends Exception {
    public EmptyAdditionException(String message) {
        super(message);
    }
}
