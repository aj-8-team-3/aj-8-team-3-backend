package app.exceptions;

public class SubscriptionCostValidationException extends Exception{
    public SubscriptionCostValidationException(String message) {
        super(message);
    }
}
