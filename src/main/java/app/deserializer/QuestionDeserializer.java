package app.deserializer;

import app.models.Question;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.List;

public class QuestionDeserializer {

    public static List<Question> loadQuestions(String pathToJsonFile) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<Question> questions = mapper.readValue(new File(pathToJsonFile), mapper.getTypeFactory().constructCollectionType(List.class, Question.class));
        return questions;
    }
}
