package app.service;

import app.models.UserSubscription;

import java.util.List;

public interface UserSubscriptionService {

    List<UserSubscription> getAll();

    UserSubscription getByUserId(String userId);

    void save(UserSubscription userSubscription);

    void delete(String id);
}
