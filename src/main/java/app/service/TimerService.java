package app.service;

import java.security.Principal;

public interface TimerService {
    void startTimer(String name);
    void stopTimer(String name);
}
