package app.service;

import app.DTOs.CompanyWithSubsDTO;
import app.exceptions.UserDoesntExistException;
import app.models.Company;

import java.util.List;

public interface CompanyService {

    List<Company> getAll();

    Company getById(String id);
    CompanyWithSubsDTO getDtoForCompanyCabinet(String bin);
    void addUser(String companyId, String employeeId, List<String> subIds) throws UserDoesntExistException;
    Company getByOwnerId(String ownerId);

    void save(Company company);

    void delete(String id);

    void createCompany(String id);
}
