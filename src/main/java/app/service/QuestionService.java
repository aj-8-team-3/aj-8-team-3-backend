package app.service;

import app.DTOs.QuestionDTO;
import app.models.Question;

import java.util.List;

public interface QuestionService {
    List<Question> getAll ();
    Question getById(String id);
    void create(QuestionDTO questionDTO);
    void update(String id, QuestionDTO questionDTO);
    void delete(String id);
}
