package app.service;

import app.models.TestType;
import app.DTOs.TestTypeDTO;
import java.util.List;

public interface TestTypeService {

    List<TestTypeDTO> getAllTestTypeDTOs();

    TestType getById(String id);

    void create(TestTypeDTO testTypeDTO);

    void delete(String id);

    void update(String id, TestTypeDTO testTypeDTO);
}
