package app.service;

import app.DTOs.SubscriptionDTO;
import app.exceptions.SubscriptionCostValidationException;
import app.models.Subscription;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface SubscriptionService {

    List<Subscription> getAll();

    Subscription getById(String id);

    void save(Subscription subscription);

    void delete(String id);

    void createSubscription(SubscriptionDTO subscriptionDTO, Authentication authentication) throws SubscriptionCostValidationException;
}
