package app.service;

import app.DTOs.EmployeeAmountDTO;
import app.models.EmployeeAmount;

import java.util.List;

public interface EmployeeAmountService {

    List<EmployeeAmount> getAll();

    void createEmployeeAmount(EmployeeAmountDTO employeeAmountDTO);

    EmployeeAmountDTO getEmployeeAmountById(String id);

    void updateEmployeeAmount(String id, EmployeeAmountDTO employeeAmountDTO);

    void deleteEmployeeAmount(String id);
}
