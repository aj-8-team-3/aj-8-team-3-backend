package app.service;

import io.minio.*;
import io.minio.http.Method;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;

public interface MinioService {

    void init() throws Exception;
    String uploadFile(String base64Data) throws Exception;
}

