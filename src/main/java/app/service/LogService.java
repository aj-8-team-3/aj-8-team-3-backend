package app.service;

import java.io.IOException;

public interface LogService {
    StringBuilder getLogs(String year, String month, String day) throws IOException;
}
