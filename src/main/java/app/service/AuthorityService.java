package app.service;

import app.models.Authority;

public interface AuthorityService {
   Authority findByAuthority(String authority);
}
