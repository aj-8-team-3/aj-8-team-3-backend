package app.service;

import app.DTOs.SubscriptionTypeDTO;

import java.util.List;

public interface SubscriptionTypeService {
    List<SubscriptionTypeDTO> getAll();
    SubscriptionTypeDTO getById(String id);
    SubscriptionTypeDTO create(SubscriptionTypeDTO subscriptionTypeDTO);
    void delete (String id);
    SubscriptionTypeDTO update(SubscriptionTypeDTO subscriptionTypeDTO, String id);
}
