package app.service;

import app.DTOs.HistoryTestDTO;
import app.DTOs.UserHistoryTestDTO;
import app.models.HistoryTest;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface HistoryTestService {

    List<HistoryTest> getAll();

    List<UserHistoryTestDTO> getAllByUserId(String userId);

    HistoryTest getById(String id);

    int create(Authentication authentication, HistoryTestDTO historyTestDTO);

    void save(HistoryTest historyTest);

    void delete(String id);
}
