package app.service;

public interface TokenService {
    void storeSessionAccessTokens(String identifier, String token);
    void storeSessionRefreshTokens(String identifier, String token);
    void storeMailValidationToken(String identifier, String token);
    void storePasswordResetToken(String identifier, String token);

    String getSessionAccessToken(String identifier);
    String getSessionRefreshToken(String identifier);
    String getMailValidationToken(String identifier);
    String getPasswordResetToken(String identifier);

    void revokeSessionAccessToken(String identifier);
    void revokeSessionRefreshToken(String identifier);
    void revokeValidationToken(String identifier);
    void revokePasswordResetToken(String identifier);
}

