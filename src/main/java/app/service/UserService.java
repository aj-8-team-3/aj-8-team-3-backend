package app.service;

import app.DTOs.AuthDTO;
import app.DTOs.PasswordDTO;
import app.DTOs.UserWithSubsDTO;
import app.exceptions.CompanyDoesntExistException;
import app.exceptions.MessageSendlerException;
import app.exceptions.UserAlreadyExistsException;
import app.exceptions.UserDoesntExistException;
import app.models.Subscription;
import app.models.User;
import jakarta.mail.MessagingException;

import java.io.IOException;
import java.util.List;

public interface UserService {
    List<User> getAll();
    User getByIdentifier(String phoneNumber);
    User findUser(String email) throws UserDoesntExistException;
    UserWithSubsDTO getDtoForCabinet(String email) throws UserDoesntExistException;
    List<Subscription> getUserSubsById(String userId);
    User getById(String id) throws UserDoesntExistException;
    void createNewUser(AuthDTO authDTO) throws UserAlreadyExistsException, MessagingException, IOException, CompanyDoesntExistException, MessageSendlerException;
    void save(User user);
    void delete(String id);
    void resetPassword(PasswordDTO passwordDTO) throws UserDoesntExistException;
    String createMailValidationToken(String email) throws MessagingException, IOException, MessageSendlerException;
    String createPasswordResetTokenForUser(User user) throws IOException, MessagingException, MessageSendlerException;
    void validateMailValidationToken(String token) throws UserDoesntExistException;
    void validatePasswordResetToken(String token);
    void createAdmin(String identifier);
    void deleteAdmin(String identifier);
    void deleteUser(String identifier);
    String getRole(User user);
}
