package app.service;

import reactor.core.publisher.Mono;


public interface BinCheckerService {
    Mono<Boolean> performPostRequest(String value);
}

