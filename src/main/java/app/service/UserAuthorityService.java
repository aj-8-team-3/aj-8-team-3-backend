package app.service;


import app.models.Authority;

import java.util.List;

public interface UserAuthorityService {
    List<Authority> getAuthoritiesByUserId(String userId);
}
