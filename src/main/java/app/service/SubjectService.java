package app.service;

import app.DTOs.SubjectDTO;
import app.models.Subject;

import java.util.List;

public interface SubjectService {

    List<Subject> getAll();
    List<Subject> getSubjectsByTestName(String testTypeName);
    Subject getById(String id);
    Subject getByTitle(String title);
    void save(Subject subject);
    void delete(String id);
    void create(SubjectDTO subjectDTO);
    void update(String id, SubjectDTO subjectDTO);
}
