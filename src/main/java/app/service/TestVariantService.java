package app.service;

import app.DTOs.*;
import app.exceptions.NoSuchTestTypeException;
import app.exceptions.SubscriptionErrorException;
import app.models.TestVariant;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface TestVariantService {

    List<TestVariantDTO> getAllTestVariantDTOs();

    TestVariant getById(String id);

    void save(TestVariant testVariant);

    void delete(String id);

    void update(String id, UpdatedTestVariantDTO updatedTestVariantDTO);

    void updateQuestion(String questionId, QuestionDTO updatedQuestionDTO);

    TestVariantDTO create(CreateTestVariantDTO createTestVariantDTO) throws NoSuchTestTypeException;

    RenderTestVariantDTO getTestForRender(ChosenTestDTO chosenTestDTO, Authentication authentication) throws SubscriptionErrorException;

    RenderSampleTestVariantDTO getSampleTestForRender(String testTypeTitle);

    int getSampleTestResult(SampleTestDTO sampleTestDTO);
}