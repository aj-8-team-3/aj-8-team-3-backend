package app.service.impl;

import app.DTOs.SubscriptionTypeDTO;
import app.mappers.SubscriptionTypeMapper;
import app.models.SubscriptionType;
import app.repositories.SubscriptionTypeRepository;
import app.service.SubscriptionTypeService;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SubscriptionTypeServiceImpl implements SubscriptionTypeService {

    private final SubscriptionTypeRepository subscriptionTypeRepository;
    private final SubscriptionTypeMapper mapper;
    private final UUIDGenerator<SubscriptionType> generator;
    private final Logger logger;

    public SubscriptionTypeServiceImpl(SubscriptionTypeRepository subscriptionTypeRepository,
                                       SubscriptionTypeMapper mapper,
                                       UUIDGenerator<SubscriptionType> generator, Logger logger) {
        this.subscriptionTypeRepository = subscriptionTypeRepository;
        this.mapper = mapper;
        this.generator = generator;
        this.logger = logger;
    }

    @Override
    public List<SubscriptionTypeDTO> getAll() {

        List<SubscriptionType> types = subscriptionTypeRepository.findAll();

        List<SubscriptionTypeDTO> collect = types
                .stream()
                .map(mapper::mapToDto)
                .toList();

        return collect;
    }

    @Override
    public SubscriptionTypeDTO  getById(String id) {
        
        Optional<SubscriptionType> subscriptionTypeOptional = subscriptionTypeRepository.findById(id);
        
        if (subscriptionTypeOptional.isEmpty()){
            throw new NoSuchElementException("There is no such a subscription type");
        }

        SubscriptionType subscriptionType = subscriptionTypeOptional.get();
        return mapper.mapToDto(subscriptionType);
    }

    @Override
    public SubscriptionTypeDTO create(SubscriptionTypeDTO subscriptionTypeDTO) {

        SubscriptionType subscriptionType = mapper.mapToEntity(subscriptionTypeDTO);
        subscriptionType.setId(generator);

        SubscriptionType saved = subscriptionTypeRepository.save(subscriptionType);
        logger.info("Новый вид подписки сохранен в базу с id:{}", subscriptionType.getId());
        return mapper.mapToDto(saved);
    }

    @Override
    public void delete(String id) {

        Optional<SubscriptionType> subscriptionTypeOptional = subscriptionTypeRepository.findById(id);
        if (subscriptionTypeOptional.isEmpty()){
            throw new NoSuchElementException("There is no such a subscription type");
        }

        SubscriptionType subscriptionType = subscriptionTypeOptional.get();
        subscriptionTypeRepository.delete(subscriptionType);
        logger.info("Вид подписки с id: {} удален из базы", subscriptionType.getId());
    }

    @Override
    public SubscriptionTypeDTO update(SubscriptionTypeDTO subscriptionTypeDTO, String id) {

        Optional<SubscriptionType> subscriptionTypeOptional = subscriptionTypeRepository.findById(id);
        if (subscriptionTypeOptional.isEmpty()){
            throw new NoSuchElementException("There is no such a subscription type");
        }

        SubscriptionType subscriptionType = subscriptionTypeOptional.get();

        subscriptionType.setPrice(subscriptionTypeDTO.getPrice());
        subscriptionType.setDuration(subscriptionTypeDTO.getDuration());

        SubscriptionType updated = subscriptionTypeRepository.save(subscriptionType);
        logger.info("Подписка с id:{} была изменена", subscriptionType.getId());
        return mapper.mapToDto(updated);
    }
}