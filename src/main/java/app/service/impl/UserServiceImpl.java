package app.service.impl;

import app.DTOs.AuthDTO;
import app.DTOs.PasswordDTO;
import app.DTOs.UserWithSubsDTO;
import app.configuration.tokenProviders.MailValidationTokenProvider;
import app.configuration.tokenProviders.PasswordResetTokenProvider;
import app.exceptions.CompanyDoesntExistException;
import app.exceptions.MessageSendlerException;
import app.exceptions.UserAlreadyExistsException;
import app.exceptions.UserDoesntExistException;
import app.models.Authority;
import app.models.Subscription;
import app.models.User;
import app.models.UserAuthority;
import app.repositories.AuthorityRepository;
import app.repositories.UserAuthorityRepository;
import app.repositories.UserRepository;
import app.service.BinCheckerService;
import app.service.HistoryTestService;
import app.service.UserService;
import app.service.UserSubscriptionService;
import app.utils.UUIDGenerator;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import jakarta.mail.MessagingException;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final UserAuthorityRepository userAuthorityRepository;
    private final PasswordResetTokenProvider passwordResetTokenProvider;
    private final MailValidationTokenProvider mailValidationTokenProvider;
    private final MailSenderService mailSender;
    private final UUIDGenerator generator;
    private final PasswordEncoder encoder;
    private final BinCheckerService binCheckerService;
    private final UserSubscriptionService userSubscriptionService;
    private final HistoryTestService historyTestService;
    private final Logger logger;
    @Value("${mail.target.url}")
    private final String targetUrl;

    public UserServiceImpl(
            UserRepository userRepository,
            AuthorityRepository authorityRepository,
            UserAuthorityRepository userAuthorityRepository,
            PasswordResetTokenProvider passwordResetTokenProvider,
            MailValidationTokenProvider mailValidationTokenProvider,
            MailSenderService mailSender,
            UUIDGenerator generator,
            PasswordEncoder encoder,
            BinCheckerService binCheckerService,
            UserSubscriptionService userSubscriptionService,
            HistoryTestService historyTestService,
            Logger logger,
            @Value("${mail.target.url}") String targetUrl
    ) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.userAuthorityRepository = userAuthorityRepository;
        this.passwordResetTokenProvider = passwordResetTokenProvider;
        this.mailValidationTokenProvider = mailValidationTokenProvider;
        this.mailSender = mailSender;
        this.generator = generator;
        this.encoder = encoder;
        this.binCheckerService = binCheckerService;
        this.userSubscriptionService = userSubscriptionService;
        this.historyTestService = historyTestService;
        this.logger = logger;
        this.targetUrl = targetUrl;
    }


    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getByIdentifier(String Identifier) {
        return userRepository.findByIdentifier(Identifier);
    }

    @Override
    public User findUser(String email) throws UserDoesntExistException {
        User user = userRepository.findByIdentifier(email);
        if (user == null) {
            throw new UserDoesntExistException("пользователь с почтой %s не зарегистрирован".formatted(email));
        }
        return user;
    }

    @Override
    public UserWithSubsDTO getDtoForCabinet(String email) {
        User user = getByIdentifier(email);
        try {
            UserWithSubsDTO userWithSubsDTO = UserWithSubsDTO
                    .builder()
                    .id(user.getId())
                    .email(user.getIdentifier())
                    .subs(userSubscriptionService.getByUserId(user.getId()).getSubscriptions())
                    .passedTests(historyTestService.getAllByUserId(user.getId()))
                    .build();
            return userWithSubsDTO;
        } catch (NullPointerException e) {
            UserWithSubsDTO userWithSubsDTO = UserWithSubsDTO
                    .builder()
                    .id(user.getId())
                    .email(user.getIdentifier())
                    .subs(new ArrayList<>())
                    .passedTests(historyTestService.getAllByUserId(user.getId()))
                    .build();
            return userWithSubsDTO;
        }
    }

    @Override
    public List<Subscription> getUserSubsById(String userId) {
        List<Subscription> subs;
        if (userSubscriptionService.getByUserId(userId) == null) {
            subs = new ArrayList<>();
        } else {
            subs = userSubscriptionService.getByUserId(userId).getSubscriptions();
        }
        return subs;
    }

    @Override
    public User getById(String id) throws UserDoesntExistException {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new UserDoesntExistException("There is no such user");
        }
        return user.get();
    }

    @Override
    public void createNewUser(AuthDTO authDTO) throws UserAlreadyExistsException, MessagingException, IOException, CompanyDoesntExistException, MessageSendlerException {

        Authority authority = authorityRepository.findByAuthority("ROLE_USER");

        UserAuthority userAuthority = new UserAuthority();

        userAuthority.setId(generator);
        userAuthority.setAuthorities(List.of(authority));

        User newUser = new User();

        if (userRepository.existsByIdentifier(authDTO.getIdentifier())) {
            throw new UserAlreadyExistsException("Email already taken: " + authDTO.getIdentifier());
        }

        newUser.setId(generator);

        newUser.setIdentifier(authDTO.getIdentifier());

        newUser.setPassword(encoder.encode(authDTO.getPassword()));

        userAuthority.setUserId(newUser.getId());

        if (newUser.getIdentifier().matches("^\\d{12}$")) {
            if (Boolean.TRUE.equals(binCheckerService.performPostRequest(newUser.getIdentifier()).block())) {
                newUser.setVerified(true);
                logger.info("Зарегистрирован новый пользователь-компания. БИН:{}",newUser.getIdentifier());
            }
            else {
                throw new CompanyDoesntExistException("Such company doesn't exist!");
            }
        }

        userAuthorityRepository.save(userAuthority);
        userRepository.save(newUser);
        logger.info("Неверифицированный пользователь сохранен в базу: {}", newUser.getIdentifier());
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public String createPasswordResetTokenForUser(User user) throws IOException, MessagingException, MessageSendlerException {
        String token = passwordResetTokenProvider.createPasswordResetToken(user.getIdentifier());


        String message = "Click the link to reset your password:";
        String url = targetUrl + "/auth/resetPassword?token=" + token;
        String secondMessage = "The link is relevant for 1.5 minutes";
        Context context = new Context();
        context.setVariable("message", message);
        context.setVariable("link", url);
        context.setVariable("second_message", secondMessage);
        mailSender.sendEmailWithHtmlTemplate(user.getIdentifier(), "Reset password", "email-template.html", context);
        return token;
    }

    @Override
    public void validateMailValidationToken(String token) throws UserDoesntExistException {
        try {
            mailValidationTokenProvider.validateToken(token);
            String identifier = mailValidationTokenProvider.getIdentifierFromMailValidationToken(token);
            User user = getByIdentifier(identifier);
            user.setVerified(true);
            save(user);
        } catch (TokenExpiredException e) {
            throw e;
        } catch (JWTVerificationException e) {
            throw e;
        }
    }

    @Override
    public void validatePasswordResetToken(String token) {
        try {
            passwordResetTokenProvider.validateToken(token);
        } catch (TokenExpiredException e) {
            throw e;
        } catch (JWTVerificationException e) {
            throw e;
        }
    }

    @Override
    public void createAdmin(String identifier) {
        User user = userRepository.findByIdentifier(identifier);
        if (user == null) {
            throw new UsernameNotFoundException("User with identifier " + identifier + " is not found");
        }
        UserAuthority userAuthority = userAuthorityRepository.findByUserId(user.getId());
        userAuthority.getAuthorities().add(authorityRepository.findByAuthority("ROLE_ADMIN"));
        userAuthorityRepository.save(userAuthority);
        logger.info("В базу сохранен пользователь {} c ролью: ROLE_ADMIN", identifier);
    }

    @Override
    public void deleteAdmin(String identifier) {
        User user = userRepository.findByIdentifier(identifier);
        if (user == null) {
            throw new UsernameNotFoundException("User with identifier " + identifier + " is not found");
        }

        UserAuthority userAuthority = userAuthorityRepository.findByUserId(user.getId());
        if (userAuthority.getAuthorities().contains(authorityRepository.findByAuthority("ROLE_ADMIN"))) {
            userRepository.delete(user);
            logger.info("Супер-админ удалил из базы пользователя {} ", identifier);
        }
        else {
            throw new NoSuchElementException("User with identifier " + identifier + " is not admin");
        }
    }

    @Override
    public void deleteUser(String identifier) {
        User user = userRepository.findByIdentifier(identifier);
        if (user == null) {
            throw new UsernameNotFoundException("User with identifier " + identifier + " is not found");
        }
        userRepository.delete(user);
        logger.info("Из базы был удален пользователь: {}", user.getIdentifier());
    }

    @Override
    public String getRole(User user) {
        UserAuthority userAuthority = userAuthorityRepository.findByUserId(user.getId());
        List<Authority> authorities = userAuthority.getAuthorities();

        if (user.getIdentifier().matches("^\\d{12}$")) {
            return "COMPANY";
        }
        else {
            for (Authority authority : authorities) {
                if (authority.getAuthority().equals("ROLE_SUPER_ADMIN")) {
                    return "SUPER_ADMIN";
                }
            }
            for (Authority authority : authorities) {
                if (authority.getAuthority().equals("ROLE_ADMIN")) {
                    return "ADMIN";
                }
            }
        }
        return "USER";
    }

    @Override
    public void resetPassword(PasswordDTO passwordDTO) throws UserDoesntExistException, TokenExpiredException, JWTVerificationException {
        try {
            User user = getByIdentifier(passwordResetTokenProvider.getIdentifierFromPasswordResetToken(passwordDTO.getToken()));
            if (user == null){
                throw new UserDoesntExistException("token isn't attached to any user of our system.");
            }
            String token = passwordResetTokenProvider.getPasswordToken(user.getIdentifier());
            if (passwordResetTokenProvider.validateToken(token)) {
                user.setPassword(encoder.encode(passwordDTO.getPassword()));
                userRepository.save(user);
                logger.info("Пользователь {} поменял пароль", user.getIdentifier());
            }
        } catch (TokenExpiredException e) {
            throw e;
        } catch (JWTVerificationException e) {
            throw e;
        }
    }

    @Override
    public String createMailValidationToken(String email) throws MessagingException, IOException, MessageSendlerException {
        String token = mailValidationTokenProvider.createMailValidationToken(email);

        String message = "Click the link to verify your account:";
        String url = targetUrl + "/verification-msg/" + token;
        String secondMessage = "The link is relevant for 1.5 minutes";
        Context context = new Context();
        context.setVariable("message", message);
        context.setVariable("link", url);
        context.setVariable("second_message", secondMessage);
        mailSender.sendEmailWithHtmlTemplate(email, "validate email", "email-template.html", context);
        return token;
    }
}