package app.service.impl;

import app.service.TokenService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.concurrent.TimeUnit;

@Service
public class TokenServiceImpl implements TokenService {

    private final RedisTemplate<String, String> redisTemplate;
    private final long accessTokenValidityInMilliseconds;
    private final long refreshTokenValidityInMilliseconds;
    private final Logger logger;
    private static final String SESSION_ACCESS_TOKEN_PREFIX = "sessionAccessToken:";
    private static final String SESSION_REFRESH_TOKEN_PREFIX = "sessionRefreshToken:";
    private static final String MAIL_VALIDATION_TOKEN_PREFIX = "mailValidationToken:";
    private static final String PASSWORD_RESET_TOKEN_PREFIX = "passwordResetToken";

    public TokenServiceImpl(RedisTemplate<String, String> redisTemplate,
                            @Value("${session.access.token.expiry.time}") long accessTokenValidityInMilliseconds,
                            @Value("${session.refresh.token.expiry.time}") long refreshTokenValidityInMilliseconds, Logger logger) {
        this.redisTemplate = redisTemplate;
        this.accessTokenValidityInMilliseconds = accessTokenValidityInMilliseconds;
        this.refreshTokenValidityInMilliseconds = refreshTokenValidityInMilliseconds;
        this.logger = logger;
    }

    @Override
    public void storeSessionAccessTokens(String identifier, String token) {
        String key = SESSION_ACCESS_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.opsForValue().set(key, token, accessTokenValidityInMilliseconds, TimeUnit.MILLISECONDS);
        logger.info("Аксесс-токен пользователя: {} сохранен в базу", identifier);
    }
    @Override
    public void storeSessionRefreshTokens(String identifier, String token) {
        String key = SESSION_REFRESH_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.opsForValue().set(key, token, refreshTokenValidityInMilliseconds, TimeUnit.MILLISECONDS);
        logger.info("Рефреш-токен пользователя: {} сохранен в базу", identifier);
    }

    @Override
    public void storeMailValidationToken(String identifier, String token) {
        String key = MAIL_VALIDATION_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.opsForValue().set(key, token, refreshTokenValidityInMilliseconds, TimeUnit.MILLISECONDS);
    }

    @Override
    public void storePasswordResetToken(String identifier, String token) {
        String key = PASSWORD_RESET_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.opsForValue().set(key, token, refreshTokenValidityInMilliseconds, TimeUnit.MILLISECONDS);
    }

    @Override
    public String getSessionAccessToken(String identifier) {
        String key = SESSION_ACCESS_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public String getSessionRefreshToken(String identifier) {
        String key = SESSION_REFRESH_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public String getMailValidationToken(String identifier) {
        String key = MAIL_VALIDATION_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public String getPasswordResetToken(String identifier) {
        String key = PASSWORD_RESET_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());;
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void revokeSessionAccessToken(String identifier) {
        String sessionKey = SESSION_ACCESS_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.delete(sessionKey);
    }

    @Override
    public void revokeSessionRefreshToken(String identifier) {
        String sessionKey = SESSION_REFRESH_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.delete(sessionKey);
    }

    @Override
    public void revokeValidationToken(String identifier) {
        String mailValidationKey = MAIL_VALIDATION_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());
        redisTemplate.delete(mailValidationKey);
    }

    @Override
    public void revokePasswordResetToken(String identifier) {
        String key = PASSWORD_RESET_TOKEN_PREFIX + Base64.getEncoder().encodeToString(identifier.getBytes());;
        redisTemplate.delete(key);
    }
}
