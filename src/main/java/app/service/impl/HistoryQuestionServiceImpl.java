package app.service.impl;

import app.models.HistoryQuestion;
import app.models.HistoryTest;
import app.repositories.HistoryQuestionRepository;
import app.service.HistoryQuestionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class HistoryQuestionServiceImpl implements HistoryQuestionService {

    private final HistoryQuestionRepository historyQuestionRepository;

    @Override
    public List<HistoryQuestion> getAll() {
        return historyQuestionRepository.findAll();
    }

    @Override
    public HistoryQuestion getById(String id) {
        Optional<HistoryQuestion> historyQuestion = historyQuestionRepository.findById(id);

        if (historyQuestion.isEmpty()) {
            throw new NoSuchElementException("There is no such historyQuestion!");
        }

        return historyQuestion.get();
    }

    @Override
    public void save(HistoryQuestion historyQuestion) {
        historyQuestionRepository.save(historyQuestion);
    }

    @Override
    public void delete(String id) {
        historyQuestionRepository.deleteById(id);
    }
}
