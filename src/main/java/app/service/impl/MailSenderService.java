package app.service.impl;

import app.exceptions.MessageSendlerException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class MailSenderService {
    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public MailSenderService(JavaMailSender mailSender, TemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    public void sendEmailWithHtmlTemplate(String to, String subject, String templateName, Context context) throws IOException, MessagingException, MessageSendlerException {
        Path imagePath = Paths.get("src/main/resources/assets/icons/logo.png");
        byte[] imageBytes = Files.readAllBytes(imagePath);

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

        try {
            helper.setFrom("Test_Platform");
            helper.setTo(to);
            helper.setSubject(subject);
            String htmlContent = templateEngine.process(templateName, context);
            helper.setText(htmlContent, true);
            helper.addInline("icon", new ByteArrayResource(imageBytes), "image/png");
            mailSender.send(mimeMessage);
        } catch (MessagingException | MailSendException e) {
            throw new MessageSendlerException(e.getMessage());
        }
    }
}