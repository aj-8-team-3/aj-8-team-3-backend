package app.service.impl;

import app.mappers.TestTypeMapper;
import app.models.TestType;
import app.repositories.TestTypeRepository;
import app.service.TestTypeService;
import app.DTOs.TestTypeDTO;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TestTypeServiceImpl implements TestTypeService {

    private final TestTypeRepository testTypeRepository;
    private final TestTypeMapper testTypeMapper;
    private final UUIDGenerator generator;
    private final Logger logger;

    @Override
    public List<TestTypeDTO> getAllTestTypeDTOs() {
        List<TestType> testTypes = testTypeRepository.findAll();
        return testTypes.stream()
                .map(TestTypeDTO::fromTestType)
                .collect(Collectors.toList());
    }

    @Override
    public TestType getById(String id) {
        Optional<TestType> testTypeOptional = testTypeRepository.findById(id);
        if (testTypeOptional.isPresent()) {
            return testTypeOptional.get();
        } else {
            throw new NoSuchElementException("TestType with id " + id + " not found.");
        }
    }

    @Override
    public void create(TestTypeDTO testTypeDTO) {
        TestType testType = testTypeMapper.mapToEntity(testTypeDTO);
        testType.setId(generator);
        testTypeRepository.save(testType);
        logger.info("Тест {} добавлен в базу", testType.getTitle());
    }

    @Override
    public void delete(String id) {
        testTypeRepository.deleteById(id);
        logger.info("Вид теста с id:{} был удален из базы", id);
    }

    @Override
    public void update(String id, TestTypeDTO testTypeDTO) {
        TestType existingTestType = getById(id);
        if (existingTestType != null) {
            existingTestType.setTitle(testTypeDTO.getTitle());
            existingTestType.setSubscriptionPrice(testTypeDTO.getSubscriptionPrice());
            existingTestType.setInstantPrice(testTypeDTO.getInstantPrice());
            testTypeRepository.save(existingTestType);
            logger.info("Тест с id:{} обновлен", id);
        } else {
            throw new NoSuchElementException("TestType with id " + id + " not found.");
        }
    }
}
