package app.service.impl;

import app.DTOs.EmployeeAmountDTO;
import app.models.EmployeeAmount;
import app.repositories.EmployeeAmountRepository;
import app.service.EmployeeAmountService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeAmountServiceImpl implements EmployeeAmountService {

    private final EmployeeAmountRepository employeeAmountRepository;
    private final Logger logger;

    @Override
    public List<EmployeeAmount> getAll() {
        return employeeAmountRepository.findAll();
    }

    @Override
    public void createEmployeeAmount(EmployeeAmountDTO employeeAmountDTO) {
        EmployeeAmount employeeAmount = EmployeeAmount.builder()
                .employeeAmount(employeeAmountDTO.getEmployeeAmount())
                .discount(employeeAmountDTO.getDiscount())
                .build();
        employeeAmountRepository.save(employeeAmount);
        logger.info("В базу данных сохранена новая константа количества сотрудников для компаний : {}",
                employeeAmount.getEmployeeAmount());
    }

    @Override
    public EmployeeAmountDTO getEmployeeAmountById(String id) {
        EmployeeAmount employeeAmount = employeeAmountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Employee amount not found"));
        return new EmployeeAmountDTO(employeeAmount.getId(), employeeAmount.getEmployeeAmount(), employeeAmount.getDiscount());
    }

    @Override
    public void updateEmployeeAmount(String id, EmployeeAmountDTO employeeAmountDTO) {
        EmployeeAmount employeeAmount = employeeAmountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Employee amount not found"));
        employeeAmount.setEmployeeAmount(employeeAmountDTO.getEmployeeAmount());
        employeeAmount.setDiscount(employeeAmountDTO.getDiscount());
        employeeAmountRepository.save(employeeAmount);
    }

    @Override
    public void deleteEmployeeAmount(String id) {
        employeeAmountRepository.deleteById(id);
        logger.info("Константа количества сотрудников для компаний с id: {} удалена", id );
    }
}

