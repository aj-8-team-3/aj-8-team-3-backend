package app.service.impl;

import app.DTOs.SubscriptionDTO;
import app.exceptions.SubscriptionCostValidationException;
import app.models.*;
import app.repositories.*;
import app.service.SubscriptionService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
@AllArgsConstructor
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final UserRepository userRepository;
    private final TestTypeRepository testTypeRepository;
    private final SubscriptionTypeRepository subscriptionTypeRepository;
    private final EmployeeAmountRepository employeeAmountRepository;
    private final UserSubscriptionRepository userSubscriptionRepository;
    private final UUIDGenerator generator;
    private final Logger logger;

    @Override
    public List<Subscription> getAll() {
        return subscriptionRepository.findAll();
    }

    @Override
    public Subscription getById(String id) {
        Optional<Subscription> subscription = subscriptionRepository.findById(id);

        if (subscription.isEmpty()) {
            throw new NoSuchElementException("There is no such subscription!");
        }

        return subscription.get();
    }

    @Override
    public void save(Subscription subscription) {
        subscriptionRepository.save(subscription);
    }

    @Override
    public void delete(String id) {
        subscriptionRepository.deleteById(id);
        logger.info("Подписка id: {} удалена из базы", id);
    }

    @Override
    public void createSubscription(SubscriptionDTO subscriptionDTO, Authentication authentication) throws SubscriptionCostValidationException{
        Subscription newSubscription = new Subscription();

        SubscriptionType subscriptionType = subscriptionTypeRepository.findSubscriptionTypeByDuration(subscriptionDTO.getSubscriptionDuration());

        EmployeeAmount employeeAmount = employeeAmountRepository.findEmployeeAmountByEmployeeAmount(subscriptionDTO.getEmployeesAmount());

        newSubscription.setId(generator);

        if (newSubscription.getTestTypes() == null) {
            newSubscription.setTestTypes(new ArrayList<>());
        }

        int costForIndividual = 0;

        for (String testTypeTitle : subscriptionDTO.getTestTypes()) {
            TestType testType = testTypeRepository.findTestTypeByTitle(testTypeTitle);
            newSubscription.getTestTypes().add(testType);
            int testCost = testType.getInstantPrice() + testType.getSubscriptionPrice() + subscriptionType.getPrice();
            costForIndividual = costForIndividual + testCost;
        }

        newSubscription.setSubscriptionType(subscriptionType);
        newSubscription.setBuyingDate(LocalDateTime.now());

        Long days = Long.valueOf(subscriptionType.getDuration());
        newSubscription.setExpireDate(LocalDateTime.now().plusDays(days));

        User user = userRepository.findByIdentifier(authentication.getName());

        if (user.getIdentifier().matches("^\\d{12}$")) {
            newSubscription.setEmployeesAmount(employeeAmount.getEmployeeAmount());
            newSubscription.setCurrentEmployees(0);
            int costForCompany = Math.round(costForIndividual - costForIndividual * employeeAmount.getDiscount()/100);

            if (subscriptionDTO.getCost() == costForCompany) {
                newSubscription.setCost(subscriptionDTO.getCost());
            }
            else {
                throw new SubscriptionCostValidationException("Subscription cost from frontend " + subscriptionDTO.getCost() + " does not match with cost on backend " + costForCompany);
            }
        }
        else {
            newSubscription.setEmployeesAmount(0);

            if (subscriptionDTO.getCost() == costForIndividual) {
                newSubscription.setCost(subscriptionDTO.getCost());
            }
            else {
                throw new SubscriptionCostValidationException("Subscription cost from frontend " + subscriptionDTO.getCost() + " does not match with cost on backend " + costForIndividual);
            }
        }

        subscriptionRepository.save(newSubscription);
        logger.info("Новая подписка сохранена в базу id: {}", newSubscription.getId());

        if (userSubscriptionRepository.findByUserId(user.getId()) == null) {
            UserSubscription newUserSubscription = new UserSubscription();
            newUserSubscription.setId(generator);
            newUserSubscription.setUserId(user.getId());

            newUserSubscription.setSubscriptions(new ArrayList<>());
            newUserSubscription.getSubscriptions().add(newSubscription);

            userSubscriptionRepository.save(newUserSubscription);
            logger.info("Подписка с id: {} приобретена пользователем: {}",
                    newUserSubscription.getId(),
                    user.getIdentifier());
        }
        else {
            UserSubscription userSubscription = userSubscriptionRepository.findByUserId(user.getId());
            userSubscription.getSubscriptions().add(newSubscription);
            userSubscriptionRepository.save(userSubscription);
            logger.info("В подписки пользователя: {} добавлена новая с id:{}",
                    user.getIdentifier(),
                    newSubscription.getId());
        }
    }
}
