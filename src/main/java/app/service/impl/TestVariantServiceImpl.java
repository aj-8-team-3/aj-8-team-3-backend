package app.service.impl;

import app.DTOs.*;
import app.exceptions.NoSuchTestTypeException;
import app.exceptions.SubscriptionErrorException;
import app.mappers.QuestionMapper;
import app.mappers.TestVariantMapper;
import app.models.*;
import app.repositories.*;
import app.service.AdditionService;
import app.service.MinioService;
import app.service.TestVariantService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TestVariantServiceImpl implements TestVariantService {

    private final TestVariantRepository testVariantRepository;
    private final QuestionRepository questionRepository;
    private final SubjectRepository subjectRepository;
    private final TestTypeRepository testTypeRepository;
    private final UserRepository userRepository;
    private final UserSubscriptionRepository userSubscriptionRepository;
    private final AdditionService additionService;
    private final HistoryTestRepository historyTestRepository;

    private final UUIDGenerator generator;

    private final QuestionMapper questionMapper;
    private final TestVariantMapper testVariantMapper;
    private final MinioService minioService;

    private final Random random;
    private final Logger logger;

    @Override
    public List<TestVariantDTO> getAllTestVariantDTOs() {
        List<TestVariant> testVariants = testVariantRepository.findAll();
        return testVariants.stream()
                .map(TestVariantDTO::fromTestVariant)
                .collect(Collectors.toList());
    }

    @Override
    public TestVariant getById(String id) {
        TestVariant testVariant = testVariantRepository.findTestVariantById(id);
        if (testVariant != null) {
            return testVariant;
        } else {
            throw new NoSuchElementException("TestVariant with id " + id + " not found.");
        }
    }

    @Override
    public void save(TestVariant testVariant) {
        testVariantRepository.save(testVariant);
        logger.info("Вариант теста с id:{} сохранен в базу", testVariant.getId());
    }

    @Override
    public void delete(String id) {
        TestVariant testVariant = getById(id);
        if (testVariant != null) {
            testVariantRepository.deleteById(id);
            logger.info("Из базы удален вариант теста с id:{}", id);
        } else {
            throw new NoSuchElementException("TestVariant with id " + id + " not found.");
        }
    }

    @Override
    public void update(String id, UpdatedTestVariantDTO updatedTestVariantDTO) {
        TestVariant existingTestVariant = getById(id);
        if (existingTestVariant != null) {
            existingTestVariant.setDescription(updatedTestVariantDTO.getDescription());
            existingTestVariant.setLanguage(updatedTestVariantDTO.getLanguage());
            existingTestVariant.setSubjectList(updatedTestVariantDTO.getSubjectList());

            testVariantRepository.save(existingTestVariant);
        } else {
            throw new NoSuchElementException("TestVariant with id " + id + " not found.");
        }
    }

    @Override
    public void updateQuestion(String questionId, QuestionDTO updatedQuestionDTO) {
        Optional<Question> optionalQuestion = questionRepository.findById(questionId);
        if (optionalQuestion.isPresent()) {
            Question question = optionalQuestion.get();

            question.setTitle(updatedQuestionDTO.getTitle());
            question.setAnswers(updatedQuestionDTO.getAnswers());
            question.setRightAnswers(updatedQuestionDTO.getRightAnswers());

            questionRepository.save(question);
        } else {
            throw new NoSuchElementException("Question with id " + questionId + " not found.");
        }
    }

    @Override
    public TestVariantDTO create(CreateTestVariantDTO createTestVariantDTO) throws NoSuchTestTypeException {

        TestType testType;

        Optional<TestType> testTypeOptional = testTypeRepository.findById(createTestVariantDTO.getTestTypeId());
        if (testTypeOptional.isEmpty()) {
            throw new NoSuchTestTypeException("No test type with such id");
        } else {
            testType = testTypeOptional.get();
        }

        List<Subject> subjectList = createTestVariantDTO.getSubjectList()
                .stream()
                .map(title -> subjectRepository.findSubjectByTitleAndTestType(title,
                        testType))
                .collect(Collectors.toList());


        TestVariant testVariant = TestVariant.builder()
                .testTypeId(createTestVariantDTO.getTestTypeId())
                .subjectList(subjectList)
                .description(createTestVariantDTO.getDescription())
                .language(createTestVariantDTO.getLanguage())
                .build();

        testVariant.setId(generator);

        createTestVariantDTO.getAdditions().forEach(additionDTO -> {
            Addition addition = new Addition();

            if (additionDTO.getContentFile() == null) {
                if (!additionDTO.getContentText().isBlank()) {
                    String contentValue = additionDTO.getContentText();

                    addition = Addition.builder()
                            .testId(testVariant.getId())
                            .subjectId(additionDTO.getSubjectId())
                            .type(additionDTO.getType())
                            .contentText(contentValue)
                            .build();
                } else {
//                    throw new EmptyAdditionException("Addition content can't be empty!");
                    System.out.println("./,l,l");
                }
            } else {
                try {

                    addition = Addition.builder()
                            .testId(testVariant.getId())
                            .subjectId(additionDTO.getSubjectId())
                            .type(additionDTO.getType())
                            .contentFile(minioService.uploadFile(additionDTO.getContentFile()))
                            .build();

                } catch (Exception e) {
                    System.out.println("./,l,l");
                }
            }

            addition.setId(generator);

            additionService.save(addition);
        });


        List<Question> questions = createTestVariantDTO.getQuestions()
                .stream()
                .map(questionMapper::mapToEntity)
                .toList();

        questions.forEach(el -> el.setId(generator));
        questions.forEach(el -> el.setTestId(testVariant.getId()));

        testVariantRepository.save(testVariant);

        questionRepository.saveAll(questions);

        return testVariantMapper.mapToDto(testVariant);
    }

    public RenderTestVariantDTO getTestForRender(ChosenTestDTO chosenTestDTO, Authentication authentication) throws SubscriptionErrorException {
        User user = userRepository.findByIdentifier(authentication.getName());

        UserSubscription userSubscription = userSubscriptionRepository.findByUserId(user.getId());

        if (userSubscription != null) {
            List<Subscription> userSubscriptionList = userSubscription.getSubscriptions();

            for (Subscription subscription : userSubscriptionList) {
                List<TestType> testTypes = subscription.getTestTypes();

                for (TestType testType : testTypes) {
                    if (testType.getId().equals(chosenTestDTO.getTestTypeId())) {
                        if (subscription.getExpireDate().isAfter(LocalDateTime.now())) {
                            String testTypeId = chosenTestDTO.getTestTypeId();
                            List<TestVariant> allByTestTypeId = testVariantRepository.findAllByTestTypeIdAndLanguage(testTypeId, chosenTestDTO.getLang());
                            List<HistoryTest> userHistoryTests = historyTestRepository.findAllByUserId(user.getId());

                            List<TestVariant> availableTestVariants = new ArrayList<>(allByTestTypeId);

                            TestVariant testVariant = new TestVariant();

                            try {
                                for (TestVariant testVariantByTestType : allByTestTypeId) {
                                    for (HistoryTest historyTest : userHistoryTests) {
                                        if (testVariantByTestType.getId().equals(historyTest.getTestId()) & chosenTestDTO.getSubjectsIds().equals(historyTest.getSubjectsList())) {
                                            availableTestVariants.remove(testVariantByTestType);
                                        }
                                    }
                                }

                                testVariant = getRandomTestVariant(availableTestVariants);
                            } catch (Exception exception) {
                                throw new NoSuchElementException("Доступных вариантов теста нет!");
                            }

                            RenderTestVariantDTO renderTestVariantDTO = new RenderTestVariantDTO();
                            renderTestVariantDTO.setTestId(testVariant.getId());
                            renderTestVariantDTO.setTestTypeId(testVariant.getTestTypeId());
                            renderTestVariantDTO.setDescription(testVariant.getDescription());
                            renderTestVariantDTO.setLanguage(testVariant.getLanguage());

                            List<String> subjectsId = chosenTestDTO.getSubjectsIds();
                            //todo если не приходит предметов то отдать весь тест

                            renderTestVariantDTO.setSubjectsList(subjectsId);

                            List<Question> questions = questionRepository.findAllByTestId(testVariant.getId());

                            List<Question> list = questions.stream()
                                    .filter(el -> subjectsId.contains(el.getSubjectId()))
                                    .toList();

                            List<RenderQuestionDTO> renderQuestionDTOList = list.stream().map(questionMapper::mapToRenderDto).toList();

                            List<Addition> additions = additionService.getAllByTestId(testVariant.getId());

                            additions = additions.stream()
                                    .filter(el -> subjectsId.contains(el.getSubjectId()))
                                    .toList();

                            renderTestVariantDTO.setAdditions(additions);

                            renderTestVariantDTO.setQuestions(renderQuestionDTOList);
                            return renderTestVariantDTO;
                        }
                    }
                }
            }
        }
        throw new SubscriptionErrorException("У вас нет подписки по тесту или срок вашей подписки истек, чтобы продолжить Вам необходимо приобрести новую подписку.");
    }

    public RenderSampleTestVariantDTO getSampleTestForRender(String testTypeTitle) {
        TestType testType = testTypeRepository.findTestTypeByTitle(testTypeTitle);

        TestVariant sampleTest = new TestVariant();

        switch (testType.getTitle()) {
            case "ЕНТ" -> sampleTest = testVariantRepository.findTestVariantByDescription("ЕНТ пробный тест");
            case "ОЗП" -> sampleTest = testVariantRepository.findTestVariantByDescription("ОЗП пробный тест");
            case "TOEFL" -> sampleTest = testVariantRepository.findTestVariantByDescription("TOEFL пробный тест");
        }

        RenderSampleTestVariantDTO renderSampleTestVariantDTO = new RenderSampleTestVariantDTO();

        renderSampleTestVariantDTO.setTestId(sampleTest.getId());
        renderSampleTestVariantDTO.setDescription(sampleTest.getDescription());

        List<String> subjectsList = new ArrayList<>();

        for (Subject subject : sampleTest.getSubjectList()) {
            subjectsList.add(subject.getId());
        }

        renderSampleTestVariantDTO.setSubjectList(subjectsList);

        List<Question> questions = questionRepository.findAllByTestId(sampleTest.getId());

        List<Addition> additions = additionService.getAllByTestId(sampleTest.getId());

        List<Question> list = questions.stream()
                .filter(el -> subjectsList.contains(el.getSubjectId()))
                .toList();

        additions = additions.stream()
                .filter(el -> subjectsList.contains(el.getSubjectId()))
                .toList();

        List<RenderQuestionDTO> renderQuestionDTOList = list.stream().map(questionMapper::mapToRenderDto).toList();

        renderSampleTestVariantDTO.setQuestions(renderQuestionDTOList);

        renderSampleTestVariantDTO.setAdditions(additions);

        return renderSampleTestVariantDTO;
    }

    @Override
    public int getSampleTestResult(SampleTestDTO sampleTestDTO) {
        int result = 0;

        for (HistoryQuestionDTO question : sampleTestDTO.getQuestions()) {
            Question existingQuestion = questionRepository.findById(question.getQuestionId()).get();
            if (question.getChosenAnswers().equals(existingQuestion.getRightAnswers())) {
                result = result + 1;
            }
        }
        return result;
    }

    public TestVariant getRandomTestVariant(List<TestVariant> testVariants) {
        int index = random.nextInt(1, testVariants.size());
        return testVariants.get(index);
    }
}