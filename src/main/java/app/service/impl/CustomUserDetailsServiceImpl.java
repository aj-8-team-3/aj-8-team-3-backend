package app.service.impl;

import app.models.CustomUserDetails;
import app.models.User;
import app.models.UserAuthority;
import app.repositories.UserAuthorityRepository;
import app.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CustomUserDetailsServiceImpl implements UserDetailsService {
    private UserRepository userRepository;
    private final UserAuthorityRepository userAuthorityRepository;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        User user = userRepository.findByIdentifier(phoneNumber);
        if(user == null) {
            throw new UsernameNotFoundException("User not found: " + phoneNumber);
        }

        UserAuthority userAuthority = userAuthorityRepository.findByUserId(user.getId());
        List<GrantedAuthority> authorities = userAuthority.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthority()))
                .collect(Collectors.toList());

        return new CustomUserDetails(user, phoneNumber, authorities);
    }
}
