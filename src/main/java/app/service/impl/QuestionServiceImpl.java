package app.service.impl;

import app.DTOs.QuestionDTO;
import app.models.Question;
import app.repositories.QuestionRepository;
import app.service.QuestionService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository repository;
    private final UUIDGenerator generator;
    private final Logger logger;
    @Override
    public List<Question> getAll() {
        return repository.findAll();
    }

    @Override
    public Question getById(String id) throws NoSuchElementException {
        Optional<Question> question = repository.findById(id);
        if (question.isEmpty()){
            logger.error("Вопрос с ID:{} не найдет в базе", id);
            throw new NoSuchElementException("There is no such question");
        }
        return question.get();
    }

    @Override
    public void create(QuestionDTO questionDTO) {
        Question question = new Question();
        question.setId(generator);
        question.setTitle(questionDTO.getTitle());
        question.setSubjectId(questionDTO.getSubjectId());
        question.setTestId(questionDTO.getTestId());
        question.setRightAnswers(questionDTO.getRightAnswers());
        for (String answer : questionDTO.getAnswers()) {
            question.getAnswers().add(answer);
        }
        for (String answer : questionDTO.getRightAnswers()) {
            question.getRightAnswers().add(answer);
        }
        repository.save(question);
        logger.info("В базу сохранен новый вопрос ID:{}", question.getId());
    }

    @Override
    public void update(String id, QuestionDTO questionDTO) {
        Question question = getById(id);
        question.setTitle(questionDTO.getTitle());
        question.setSubjectId(question.getSubjectId());
        question.setTestId(questionDTO.getTestId());
        question.setRightAnswers(questionDTO.getRightAnswers());
        question.setAnswers(questionDTO.getAnswers());
        repository.save(question);
        logger.info("Вопрос с ID: {} обновлен", id);
    }


    @Override
    public void delete(String id) {
        repository.deleteById(id);
        logger.info("Вопрос с ID:{} удален из базы", id);
    }
}
