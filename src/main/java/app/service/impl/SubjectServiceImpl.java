package app.service.impl;

import app.DTOs.SubjectDTO;
import app.models.Subject;
import app.models.TestType;
import app.repositories.SubjectRepository;
import app.repositories.TestTypeRepository;
import app.service.SubjectService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;

    private final TestTypeRepository testTypeRepository;

    private final UUIDGenerator generator;
    private final Logger logger;

    @Override
    public List<Subject> getAll() {
        return subjectRepository.findAll();
    }

    @Override
    public List<Subject> getSubjectsByTestName(String testTypeName) {
        TestType testType = testTypeRepository.findTestTypeByTitle(testTypeName);

        if (testType == null) {
            logger.error("Не удалось загрузить предметы по виду теста так как вид теста с названием: {} не найден в базе ");
            throw new NoSuchElementException("No such test type!");
        }

        Optional<List<Subject>> subjects = subjectRepository.findAllByTestType(testType);

        return subjects.orElseGet(ArrayList::new);

    }

    @Override
    public Subject getById(String id) {
        Optional<Subject> subject = subjectRepository.findById(id);

        if (subject.isEmpty()) {
            logger.error("Не удалось получить предмет. Предмет с ID:{} отсутсвует в базе", id);
            throw new NoSuchElementException("There is no such subject!");
        }

        return subject.get();
    }

    @Override
    public Subject getByTitle(String title) {
        Subject subject = subjectRepository.findSubjectByTitle(title);

        if (subject == null) {
            logger.error("Не удалось найти предмет в базе с названием:{}", title);
            throw new NoSuchElementException("There is no such subject!");
        }

        return subject;
    }

    @Override
    public void save(Subject subject) {
        subjectRepository.save(subject);
        logger.info("Предмет с ID:{} сохранен в базу", subject.getId());
    }

    @Override
    public void delete(String id) {
        subjectRepository.deleteById(id);
        logger.info("Предмет с ID:{} удален из базы", id);
    }

    @Override
    public void create(SubjectDTO subjectDTO) {

        Subject newSubject = new Subject();

        newSubject.setId(generator);
        newSubject.setTitle(subjectDTO.getTitle());

        TestType testType = testTypeRepository.findTestTypeByTitle(subjectDTO.getTestType());
        newSubject.setTestType(testType);
        newSubject.setDependencies(new ArrayList<>());

        for (String subjectTitle : subjectDTO.getDependencies()) {
            Subject dependentSubject = subjectRepository.findSubjectByTitle(subjectTitle);
            newSubject.getDependencies().add(dependentSubject.getId());
        }

        newSubject.setRequired(Boolean.parseBoolean(subjectDTO.getIsRequired()));
        subjectRepository.save(newSubject);
        logger.info("Предмет с ID:{} сохранен в базу", newSubject.getId() );
    }

    @Override
    public void update(String id, SubjectDTO subjectDTO) {
        Subject subject = getById(id);

        subject.setTitle(subjectDTO.getTitle());
        TestType testType = testTypeRepository.findTestTypeByTitle(subjectDTO.getTestType());
        subject.setTestType(testType);
        subject.setDependencies(new ArrayList<>());

        for (String subjectTitle : subjectDTO.getDependencies()) {
            Subject dependentSubject = subjectRepository.findSubjectByTitle(subjectTitle);
            subject.getDependencies().add(dependentSubject.getId());
        }

        subject.setRequired(Boolean.parseBoolean(subjectDTO.getIsRequired()));
        subjectRepository.save(subject);
        logger.info("Предмет с ID:{} был изменен и сохранен в базу", id);
    }
}
