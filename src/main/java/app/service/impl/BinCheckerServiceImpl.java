package app.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class BinCheckerServiceImpl implements app.service.BinCheckerService {

    private final WebClient webClient;

    @Autowired
    public BinCheckerServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://pk.uchet.kz").build();
    }

    @Override
    public Mono<Boolean> performPostRequest(String value) {
        return webClient.post()
                .uri("/api/web/company/search/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .bodyValue("value=" + value)
                .retrieve()
                .bodyToMono(JsonNode.class)
                .map(jsonNode -> !jsonNode.get("results").isEmpty());
    }
}

