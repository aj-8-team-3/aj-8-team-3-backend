package app.service.impl;

import app.service.LogService;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


@Service
public class LogServiceImpl implements LogService {

    private final ResourceLoader resourceLoader;
    private final Logger logger;

    public LogServiceImpl(ResourceLoader resourceLoader, Logger logger) {
        this.resourceLoader = resourceLoader;
        this.logger = logger;
    }

    @Override
    public StringBuilder getLogs(String year, String month, String day) throws IOException {
        StringBuilder result = new StringBuilder();
        String location = String.format("file:logs/%s/%s/%s.log", year, month, day);

        Resource resource = resourceLoader.getResource(location);
        try(InputStream inputStream = resource.getInputStream()){
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            reader.lines().forEach(el -> {
                result.append(el).append(" <br>").append("\n");
            });
        }
        logger.info("Отправлены логи за {}.{}.{}", day, month, year);
        return result;
    }
}
