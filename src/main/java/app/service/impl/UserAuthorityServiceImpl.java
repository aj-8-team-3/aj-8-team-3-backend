package app.service.impl;

import app.models.Authority;
import app.models.UserAuthority;
import app.repositories.UserAuthorityRepository;
import app.service.UserAuthorityService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserAuthorityServiceImpl implements UserAuthorityService {
    private final UserAuthorityRepository userAuthorityRepository;
    @Override
    public List<Authority> getAuthoritiesByUserId(String userId) {
        UserAuthority userAuthority = userAuthorityRepository.getUserAuthoritiesByUserId(userId);
        return userAuthority.getAuthorities();
    }
}
