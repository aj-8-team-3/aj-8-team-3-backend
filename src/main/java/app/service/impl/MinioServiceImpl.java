package app.service.impl;

import app.service.MinioService;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.http.Method;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import jakarta.annotation.PostConstruct;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

@Service
public class MinioServiceImpl implements MinioService {
    @Value("${minio.access.name}")
    private String accessKey;

    @Value("${minio.access.secret}")
    private String accessSecret;

    @Value("${minio.url}")
    private String minioUrl;

    @Value("${minio.bucket.name}")
    private String bucketName;

    private MinioClient minioClient;

    @PostConstruct
    @Override
    public void init() throws Exception {
        minioClient = MinioClient.builder()
                .endpoint(minioUrl)
                .credentials(accessKey, accessSecret)
                .build();

        if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }
    }

    @Override
    public String uploadFile(String base64String) throws Exception {

        String[] chunks = base64String.split(",");
        String base64Data = chunks[1];
        String metadata = chunks[0];
        String mimeType = metadata.split(";")[0].split(":")[1];
        String extension = mimeType.split("/")[1];

        String fileName = "file_" + System.currentTimeMillis() + "." + extension;

        byte[] decodedBytes = Base64.getDecoder().decode(base64Data);

        ByteArrayInputStream baits = new ByteArrayInputStream(decodedBytes);

        minioClient.putObject(
                PutObjectArgs.builder().bucket(bucketName).object(fileName).stream(
                                baits, decodedBytes.length, 10485760)
                        .contentType(mimeType)
                        .build()
        );

        return minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(bucketName)
                        .object(fileName)
                        .build()
        );
    }
}

