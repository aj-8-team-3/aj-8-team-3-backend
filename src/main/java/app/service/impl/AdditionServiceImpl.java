package app.service.impl;

import app.models.Addition;
import app.repositories.AdditionRepository;
import app.service.AdditionService;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdditionServiceImpl implements AdditionService {
    private final AdditionRepository additionRepository;

    @Override
    public void save(Addition addition) {
        additionRepository.save(addition);
    }

    @Override
    public List<Addition> getAllByTestId(String testId) {
        return additionRepository.findAllByTestId(testId);
    }
}
