package app.service.impl;

import app.DTOs.CompanyWithSubsDTO;
import app.exceptions.UserDoesntExistException;
import app.models.Company;
import app.models.Subscription;
import app.models.User;
import app.models.UserSubscription;
import app.repositories.CompanyRepository;
import app.repositories.UserRepository;
import app.service.CompanyService;
import app.service.SubscriptionService;
import app.service.UserService;
import app.service.UserSubscriptionService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    private final UUIDGenerator generator;

    private final Logger logger;
    private final UserService userService;
    private final UserSubscriptionService userSubscriptionService;
    private final SubscriptionService subscriptionService;
    private final UserRepository userRepository;

    @Override
    public List<Company> getAll() {
        return companyRepository.findAll();
    }

    @Override
    public Company getById(String id) {
        Optional<Company> company = companyRepository.findById(id);

        if (company.isEmpty()) {

            throw new NoSuchElementException("There is no such company!");
        }

        return company.get();
    }

    @Override
    public CompanyWithSubsDTO getDtoForCompanyCabinet(String bin) {
        User owner = userRepository.findByIdentifier(bin);
        Company company = getByOwnerId(owner.getId());

        List<Subscription> subs = new ArrayList<>();
        if (userSubscriptionService.getByUserId(owner.getId()) != null) {
            subs = userSubscriptionService.getByUserId(owner.getId()).getSubscriptions();
        }

        CompanyWithSubsDTO companyWithSubsDTO = CompanyWithSubsDTO
                .builder()
                .id(company.getId())
                .bin(bin)
                .subs(subs)
                .employees(company.getEmployees())
                .build();
        return companyWithSubsDTO;
    }

    @Override
    public void addUser(String companyId, String employeeId, List<String> subIds) throws NullPointerException, UserDoesntExistException {
        Company company = getById(companyId);
        User employee = userService.getById(employeeId);
        UserSubscription employeeSubscription = userSubscriptionService.getByUserId(employeeId);


        if (employeeSubscription == null) {
            employeeSubscription = UserSubscription
                    .builder()
                    .userId(employeeId)
                    .subscriptions(new ArrayList<>())
                    .build();
            employeeSubscription.setId(generator);
            userSubscriptionService.save(employeeSubscription);
        }


        List<Subscription> employeesSubs = employeeSubscription.getSubscriptions();
        List<User> employees = company.getEmployees();
        UserSubscription companySubscription = userSubscriptionService.getByUserId(company.getOwnerId());

        List<Subscription> subs = companySubscription.getSubscriptions();
        subIds.forEach(subId -> {
            subs.forEach(sub -> {
                if (Objects.equals(sub.getId(), subId)) {
                    sub.setCurrentEmployees(sub.getCurrentEmployees() + 1);
                    subscriptionService.save(sub);
                    employeesSubs.add(sub);
                }
            });
        });

        companySubscription.setSubscriptions(subs);
        userSubscriptionService.save(companySubscription);
        employeeSubscription.setSubscriptions(employeesSubs);
        userSubscriptionService.save(employeeSubscription);

        List foundEmployees = employees.stream().filter(emp -> emp.getId().equals(employeeId)).collect(Collectors.toList());
        if (foundEmployees.size() == 0) {
            employees.add(employee);
        }
        company.setEmployees(employees);
        save(company);
    }

    @Override
    public Company getByOwnerId(String ownerId) {
        return companyRepository.findByOwnerId(ownerId);
    }

    @Override
    public void save(Company company) {
        companyRepository.save(company);
    }

    @Override
    public void delete(String id) {
        companyRepository.deleteById(id);
        logger.info("Компания с id:{} была удалена", id);
    }

    @Override
    public void createCompany(String id) {
        Company company = new Company();

        company.setId(generator);
        company.setOwnerId(id);
        company.setEmployees(new ArrayList<>());

        companyRepository.save(company);
        logger.info("Компания с id: {} , сохранена в базу. ownerId:{}  ", company.getId(), id);
    }
}
