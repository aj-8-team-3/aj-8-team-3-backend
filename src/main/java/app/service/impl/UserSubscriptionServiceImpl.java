package app.service.impl;

import app.models.UserSubscription;
import app.repositories.UserSubscriptionRepository;
import app.service.UserSubscriptionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserSubscriptionServiceImpl implements UserSubscriptionService {

    private final UserSubscriptionRepository userSubscriptionRepository;

    @Override
    public List<UserSubscription> getAll() {
        return userSubscriptionRepository.findAll();
    }

    @Override
    public UserSubscription getByUserId(String userId) {
        return userSubscriptionRepository.findByUserId(userId);
    }

    @Override
    public void save(UserSubscription userSubscription) {
        userSubscriptionRepository.save(userSubscription);
    }

    @Override
    public void delete(String id) {
        userSubscriptionRepository.deleteById(id);
    }
}
