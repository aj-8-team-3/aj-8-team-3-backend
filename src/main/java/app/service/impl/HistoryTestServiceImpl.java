package app.service.impl;

import app.DTOs.HistoryQuestionDTO;
import app.DTOs.HistoryTestDTO;
import app.DTOs.UserHistoryTestDTO;
import app.models.*;
import app.repositories.*;
import app.service.HistoryTestService;
import app.utils.UUIDGenerator;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class HistoryTestServiceImpl implements HistoryTestService {

    private final HistoryTestRepository historyTestRepository;
    private final HistoryQuestionRepository historyQuestionRepository;
    private final UserRepository userRepository;
    private final QuestionRepository questionRepository;
    private final TestVariantRepository testVariantRepository;
    private final SubjectRepository subjectRepository;
    private final UUIDGenerator generator;
    private final Logger logger;

    @Override
    public List<HistoryTest> getAll() {
        return historyTestRepository.findAll();
    }

    @Override
    public List<UserHistoryTestDTO> getAllByUserId(String userId) {
        List<HistoryTest> passedTests = historyTestRepository.findAllByUserId(userId);
        List<UserHistoryTestDTO> passedTestsDTO = new ArrayList<>();

        for (HistoryTest test : passedTests) {
            List<String> subjects = new ArrayList<>();

            for (String subjectId : test.getSubjectsList()) {
                Subject subject = subjectRepository.findById(subjectId).get();
                subjects.add(subject.getTitle());
            }

            UserHistoryTestDTO userHistoryTestDTO = UserHistoryTestDTO
                    .builder()
                    .title(testVariantRepository.findTestVariantById(test.getTestId()).getDescription())
                    .subjects(subjects)
                    .submissionDate(test.getSubmissionDate())
                    .result(test.getResult())
                    .build();
            passedTestsDTO.add(userHistoryTestDTO);
        }

        return passedTestsDTO;
    }

    @Override
    public HistoryTest getById(String id) {
        Optional<HistoryTest> historyTest = historyTestRepository.findById(id);

        if (historyTest.isEmpty()) {
            throw new NoSuchElementException("There is no such historyTest!");
        }

        return historyTest.get();
    }

    @Override
    public int create(Authentication authentication, HistoryTestDTO historyTestDTO) {
        HistoryTest historyTest = new HistoryTest();

        User user = userRepository.findByIdentifier(authentication.getName());

        historyTest.setId(generator);
        historyTest.setUserId(user.getId());
        historyTest.setTestId(historyTestDTO.getTestId());
        historyTest.setSubjectsList(historyTestDTO.getSubjectsList());
        historyTest.setResult(0);
        historyTest.setSubmissionDate(LocalDateTime.now());
        historyTestRepository.save(historyTest);

        for (HistoryQuestionDTO question : historyTestDTO.getQuestions()) {
            HistoryQuestion historyQuestion = new HistoryQuestion();
            historyQuestion.setId(generator);
            historyQuestion.setHistoryTestId(historyTest.getId());
            historyQuestion.setQuestionId(question.getQuestionId());
            historyQuestion.setChosenAnswers(question.getChosenAnswers());
            Question existingQuestion = questionRepository.findById(question.getQuestionId()).get();
            historyQuestionRepository.save(historyQuestion);
            if (question.getChosenAnswers().equals(existingQuestion.getRightAnswers())) {
                historyTest.setResult(historyTest.getResult() + 1);
                historyTestRepository.save(historyTest);
            }
        }
        logger.info("В базу сохранен результат прохождения теста id:{},пользователем: {}  ",
                historyTest.getTestId(),
                user.getIdentifier());

        return historyTest.getResult();
    }

    @Override
    public void save(HistoryTest historyTest) {
        historyTestRepository.save(historyTest);
    }

    @Override
    public void delete(String id) {
        historyTestRepository.deleteById(id);
        logger.info("Тест id:{} удален из базы", id);
    }
}
