package app.service;

import app.models.Addition;

import java.util.List;

public interface AdditionService {
    void save(Addition addition);
    List<Addition> getAllByTestId(String testId);
}
