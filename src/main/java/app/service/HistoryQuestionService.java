package app.service;

import app.models.HistoryQuestion;

import java.util.List;

public interface HistoryQuestionService {

    List<HistoryQuestion> getAll();

    HistoryQuestion getById(String id);

    void save(HistoryQuestion historyQuestion);

    void delete(String id);
}
