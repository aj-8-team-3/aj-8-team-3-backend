package app.controllers;

import app.service.LogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/logs")
@CrossOrigin(origins = "http://localhost:8080")
public class LogsController {

    private final LogService logService;

    public LogsController(LogService logService) {
        this.logService = logService;
    }


    @GetMapping("/{year}/{month}/{day}")
    public ResponseEntity<StringBuilder> getLogs(@PathVariable String year,
                                          @PathVariable String month,
                                          @PathVariable String day) throws IOException {
        StringBuilder logs = logService.getLogs(year, month, day);
        return ResponseEntity.ok(logs);
    }
}
