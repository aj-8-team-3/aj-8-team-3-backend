package app.controllers;

import app.service.TimerService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class TimerController {
    private final TimerService timerService;

    public TimerController(TimerService timerService) {
        this.timerService = timerService;
    }

    @MessageMapping("/startTimer")
    public void startTimerForUser(Principal principal){
        System.out.println("Invoke startTimerForUser");
        timerService.startTimer(principal.getName());
    }

    @GetMapping("/stopTimer")
    @ResponseBody
    public void stopTimerForUser(Principal principal) {
        System.out.println("Invoke stopTimerForUser");
        timerService.stopTimer(principal.getName());
    }
}