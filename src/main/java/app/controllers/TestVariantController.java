package app.controllers;

import app.DTOs.*;
import app.exceptions.NoSuchTestTypeException;
import app.models.TestVariant;
import app.service.TestVariantService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/test-variants")
@CrossOrigin(origins = "http://localhost:8080")
public class TestVariantController {

    private final TestVariantService testVariantService;

    @GetMapping
    public ResponseEntity<?> getAllTestVariants() {
        return ResponseEntity.ok(new Response<>("", testVariantService.getAllTestVariantDTOs(), 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTestVariantById(@PathVariable String id) {
        TestVariant testVariant = testVariantService.getById(id);
        return ResponseEntity.ok(new Response<>("", TestVariantDTO.fromTestVariant(testVariant), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> deleteTestVariant(@PathVariable String id) {
        testVariantService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> updateTestVariant(@PathVariable String id, @Valid @RequestBody UpdatedTestVariantDTO updatedTestVariantDTO) {
        testVariantService.update(id, updatedTestVariantDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PutMapping("/{testVariantId}/questions/{questionId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> updateQuestionInTestVariant(@PathVariable String testVariantId, @PathVariable String questionId, @Valid @RequestBody QuestionDTO updatedQuestionDTO) {
        testVariantService.updateQuestion(questionId, updatedQuestionDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response<TestVariantDTO>> createTest(@Valid @RequestBody CreateTestVariantDTO createTestVariantDTO) throws NoSuchTestTypeException {
        TestVariantDTO testVariantDTO = testVariantService.create(createTestVariantDTO);

        Response<TestVariantDTO> response = new Response<>(
                "",
                testVariantDTO,
                HttpStatus.CREATED.value(),
                true
        );

        return ResponseEntity.ok(response);
    }

    @PostMapping("/render")
    public  ResponseEntity<?> getTestForRender(@RequestBody ChosenTestDTO chosenTestDTO, Authentication authentication) {
        try {
            RenderTestVariantDTO testForRender = testVariantService.getTestForRender(chosenTestDTO, authentication);

            Response<RenderTestVariantDTO> response = new Response<>(
                    "",
                    testForRender,
                    HttpStatus.OK.value(),
                    true
            );

            return ResponseEntity.ok(response);
        }
        catch (Exception exception) {
            return ResponseEntity.ok(new Response<>("", exception.getMessage(), 409, true));
        }
    }
}