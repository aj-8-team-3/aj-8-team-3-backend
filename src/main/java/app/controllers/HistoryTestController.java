package app.controllers;

import app.DTOs.HistoryTestDTO;
import app.DTOs.Response;
import app.service.HistoryTestService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/history-tests")
@CrossOrigin(origins = "http://localhost:8080")
public class HistoryTestController {

    private final HistoryTestService historyTestService;

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody HistoryTestDTO historyTestDTO, Authentication authentication) {
        return ResponseEntity.ok(new Response<>("", historyTestService.create(authentication, historyTestDTO), 200, true));
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", historyTestService.getAll(), 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getHistoryTest(@PathVariable String id)     {
        return ResponseEntity.ok(new Response<>("", historyTestService.getById(id), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        historyTestService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
