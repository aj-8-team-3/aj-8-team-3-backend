package app.controllers;

import app.DTOs.AddEmployeeDTO;
import app.DTOs.Response;
import app.exceptions.UserDoesntExistException;
import app.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/companies")
@CrossOrigin(origins = "http://localhost:8080")
public class CompanyController {

    private final CompanyService companyService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", companyService.getAll(), 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCompany(@PathVariable String id) {
        return ResponseEntity.ok(new Response<>("", companyService.getById(id), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        companyService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PostMapping()
    public ResponseEntity<?> addUser(@RequestBody AddEmployeeDTO addEmployeeDTO) throws UserDoesntExistException {
        companyService.addUser(addEmployeeDTO.getCompanyId(), addEmployeeDTO.getEmployeeId(), addEmployeeDTO.getSubIds());
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
