package app.controllers;

import app.DTOs.Response;
import app.DTOs.TestTypeDTO;
import app.models.TestType;
import app.service.TestTypeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/test-types")
@CrossOrigin(origins = "http://localhost:8080")

public class TestTypeController {

    private final TestTypeService testTypeService;

    @GetMapping("/all")
    public ResponseEntity<?> getAllTestTypes() {
        List<TestTypeDTO> testTypeDTOS = testTypeService.getAllTestTypeDTOs();
        return ResponseEntity.ok(new Response<>("", testTypeDTOS, 200, true));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody TestTypeDTO testTypeDTO) {
        testTypeService.create(testTypeDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTestType(@PathVariable String id) {
        TestType testType = testTypeService.getById(id);
        return ResponseEntity.ok(new Response<>("", TestTypeDTO.fromTestType(testType), 200, true));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> update(@PathVariable String id, @Valid @RequestBody TestTypeDTO testTypeDTO) {
        testTypeService.update(id, testTypeDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        testTypeService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
