package app.controllers;

import app.DTOs.AuthDTO;
import app.DTOs.PasswordDTO;
import app.DTOs.Response;
import app.configuration.tokenProviders.SessionJwtTokenProvider;
import app.exceptions.CompanyDoesntExistException;
import app.exceptions.MessageSendlerException;
import app.exceptions.UserAlreadyExistsException;
import app.exceptions.UserDoesntExistException;
import app.models.User;
import app.service.CompanyService;
import app.service.TokenService;
import app.service.UserAuthorityService;
import app.service.UserService;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import jakarta.mail.MessagingException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/auth")
@CrossOrigin(origins = "http://localhost:8080")
public class AuthController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final SessionJwtTokenProvider sessionJwtTokenProvider;
    private final TokenService tokenService;
    private final UserAuthorityService userAuthorityService;
    private final CompanyService companyService;

    @PostMapping("/authorize")
    public ResponseEntity<?> authorizeUser(@Valid @RequestBody AuthDTO authDTO,
                                           @RequestHeader("User-Agent") String device,
                                           HttpServletResponse response) throws UserAlreadyExistsException, MessagingException, IOException, CompanyDoesntExistException, MessageSendlerException {

        if (userService.getByIdentifier(authDTO.getIdentifier()) == null) {
            userService.createNewUser(authDTO);
            if (companyService.getByOwnerId(userService.getByIdentifier(authDTO.getIdentifier()).getId()) == null && authDTO.getIdentifier().matches("^\\d{12}$")) {
                companyService.createCompany(userService.getByIdentifier(authDTO.getIdentifier()).getId());
            }
        }

        User user = userService.getByIdentifier(authDTO.getIdentifier());

        try {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    authDTO.getIdentifier(),
                    authDTO.getPassword(),
                    userAuthorityService.getAuthoritiesByUserId(user.getId()));
            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String accessToken = sessionJwtTokenProvider.createSessionTokens(authentication, device, true);

            String accessCookieHeader = String.format(
                    "accessToken=%s; Path=/; Max-Age=172800; SameSite=Strict",
                    accessToken
            );
            response.addHeader("Set-Cookie", accessCookieHeader);

            String token = sessionJwtTokenProvider.createSessionTokens(authentication, device, false);

            String refreshCookieHeader = String.format(
                    "refreshToken=%s; Path=/; Max-Age=172800; SameSite=Strict",
                    token
            );
            response.addHeader("Set-Cookie", refreshCookieHeader);

            if (user.getVerified()) {
                return ResponseEntity.ok(new Response<>("", "Authentication successful", HttpStatus.OK.value(), true));
            }
            else {
                return ResponseEntity.ok(new Response<>("Verification link was sent to your e-mail: " + user.getIdentifier(), "" , HttpStatus.MOVED_PERMANENTLY.value(), true));
            }
        } catch (AuthenticationException e) {
            return ResponseEntity.ok(new Response<>("Authentication failed", null, HttpStatus.UNAUTHORIZED.value(), false));
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        String accessToken = null, refreshToken = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("accessToken".equals(cookie.getName())) {
                    accessToken = cookie.getValue();
                }
                else if ("refreshToken".equals(cookie.getName())) {
                    refreshToken = cookie.getValue();
                    break;
                }
            }
        }

        if (accessToken != null && !accessToken.isEmpty()
        && refreshToken != null && !refreshToken.isEmpty()) {
            String phoneNumber = sessionJwtTokenProvider.getIdentifierFromSessionToken(refreshToken);
            tokenService.revokeSessionAccessToken(phoneNumber);
            tokenService.revokeSessionRefreshToken(phoneNumber);
        }

        SecurityContextHolder.clearContext();

        Cookie accessCookie = new Cookie("accessToken", null);
        Cookie refreshCookie = new Cookie("refreshToken", null);
        accessCookie.setPath("/");
        refreshCookie.setPath("/");
        accessCookie.setHttpOnly(true);
        refreshCookie.setHttpOnly(true);
        accessCookie.setSecure(true);
        refreshCookie.setSecure(true);
        accessCookie.setMaxAge(0);
        refreshCookie.setMaxAge(0);
        response.addCookie(accessCookie);
        response.addCookie(refreshCookie);

        return ResponseEntity.ok(new Response<>("", "Logged out successfully", HttpStatus.OK.value(), true));
    }



    @GetMapping("/check_device")
    public ResponseEntity<Response<Boolean>> checkDevice(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        String accessToken = null, refreshToken = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("accessToken".equals(cookie.getName())) {
                    accessToken = cookie.getValue();
                }
                else if ("refreshToken".equals(cookie.getName())) {
                    refreshToken = cookie.getValue();
                }
            }
        }

        if (accessToken == null || accessToken.isEmpty() || refreshToken == null || refreshToken.isEmpty()) {
            return ResponseEntity.ok(new Response<>("Токен не предоставлен", null, HttpStatus.UNAUTHORIZED.value(), false));
        }

        try {
            String identifier = sessionJwtTokenProvider.getIdentifierFromSessionToken(accessToken);
            String currentDevice = sessionJwtTokenProvider.getDeviceFromToken(accessToken);
            String storedToken = sessionJwtTokenProvider.getSessionAccessTokens(identifier);

            if (storedToken == null) {
                return ResponseEntity.ok(new Response<>("Токен не хранится", null, HttpStatus.UNAUTHORIZED.value(), false));
            }

            String storedDevice = sessionJwtTokenProvider.getDeviceFromToken(storedToken);

            boolean isSameDevice = currentDevice.equals(storedDevice);

            if (!isSameDevice) {
                Cookie accessCookie = new Cookie("accessToken", null);
                Cookie refreshCookie = new Cookie("refreshToken", null);
                accessCookie.setPath("/");
                refreshCookie.setPath("/");
                accessCookie.setHttpOnly(true);
                refreshCookie.setHttpOnly(true);
                accessCookie.setSecure(true);
                refreshCookie.setSecure(true);
                accessCookie.setMaxAge(0);
                refreshCookie.setMaxAge(0);
                response.addCookie(accessCookie);
                response.addCookie(refreshCookie);
            }

            return ResponseEntity.ok(new Response<>("", isSameDevice, HttpStatus.OK.value(), true));
        } catch (Exception e) {
            return ResponseEntity.ok(new Response<>("Got an exception while validating token", false, HttpStatus.OK.value(), false));
        }
    }


    @GetMapping("/getMailValidationToken")
    public ResponseEntity<?> resendMailValidationToken(@RequestParam("email") String email) throws MessagingException, IOException, MessageSendlerException {
        String token = userService.createMailValidationToken(email);
        return ResponseEntity.ok(new Response<>("", "sent mail validation link to client: %s".formatted(token), 200, true));
    }
    @GetMapping("/getMailValidationTokenByToken")
    public ResponseEntity<?> resendMailValidationTokenByToken(Authentication authentication) throws MessagingException, IOException, MessageSendlerException {
        String token = userService.createMailValidationToken(authentication.getName());
        return ResponseEntity.ok(new Response<>("", "sent mail validation link to client: %s".formatted(token), 200, true));
    }

    @GetMapping("/verifyEmail/{token}")
    public ResponseEntity<?> verifyEmail(@PathVariable(name = "token") String token){
        try {
            userService.validateMailValidationToken(token);
            tokenService.revokeValidationToken(token);
            return ResponseEntity.ok(new Response<String>("","account verified!", 200, true));
        } catch (TokenExpiredException e) {
            return ResponseEntity.ok(new Response<>("Token expired", null, HttpStatus.UNAUTHORIZED.value(), false));
        } catch (JWTVerificationException | UserDoesntExistException e) {
            return ResponseEntity.ok(new Response<>("Invalid token", null, HttpStatus.UNAUTHORIZED.value(), false));
        }
    }

    @GetMapping("/getPasswordResetToken")
    public ResponseEntity<?> sendPasswordResetToken(@RequestParam("email") String email) throws UserDoesntExistException, IOException, MessagingException, MessageSendlerException {
        User user = userService.getByIdentifier(email);
        if (user == null) {
            throw new UserDoesntExistException("user with email %s doesn't exist".formatted(email));
        }
        String token = userService.createPasswordResetTokenForUser(user);

        return ResponseEntity.ok(new Response<>("", "sent password reset link to client: %s".formatted(token), HttpStatus.OK.value(), false));
    }
    @GetMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestParam("token") String token) throws TokenExpiredException, JWTVerificationException {
        try {
            userService.validatePasswordResetToken(token);
            tokenService.revokePasswordResetToken(token);
            return ResponseEntity.ok(new Response<String>("","redirect to password change page", 200, true));
        } catch (TokenExpiredException e) {
            return ResponseEntity.ok(new Response<>("Token expired", null, HttpStatus.UNAUTHORIZED.value(), false));
        } catch (JWTVerificationException e) {
            return ResponseEntity.ok(new Response<>("Invalid token", null, HttpStatus.UNAUTHORIZED.value(), false));
        }
    }
    @PostMapping("/savePassword")
    public ResponseEntity<?> savePassword(@RequestBody @Valid PasswordDTO passwordDTO) throws UserDoesntExistException {
        try {
            userService.resetPassword(passwordDTO);
            return ResponseEntity.ok(new Response<>("", "Refreshed password successfully.", HttpStatus.OK.value(), true));
        } catch (TokenExpiredException e) {
            return ResponseEntity.ok(new Response<>("Token expired", null, HttpStatus.UNAUTHORIZED.value(), false));
        } catch (JWTVerificationException e) {
            return ResponseEntity.ok(new Response<>("Invalid token", null, HttpStatus.UNAUTHORIZED.value(), false));
        }
    }

    @GetMapping("/isVerified")
    public ResponseEntity<?> isVerified() throws UserDoesntExistException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String identifier = authentication.getName();

        return ResponseEntity.ok(new Response<>("", userService.getByIdentifier(identifier).getVerified(), HttpStatus.OK.value(), true));
    }

}
