package app.controllers;

import app.DTOs.Response;
import app.service.HistoryQuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/history-questions")
@CrossOrigin(origins = "http://localhost:8080")
public class HistoryQuestionController {

    private final HistoryQuestionService historyQuestionService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", historyQuestionService.getAll(), 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getHistoryTest(@PathVariable String id)     {
        return ResponseEntity.ok(new Response<>("", historyQuestionService.getById(id), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        historyQuestionService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
