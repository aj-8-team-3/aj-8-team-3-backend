package app.controllers;

import app.DTOs.Response;
import app.DTOs.SubscriptionDTO;
import app.exceptions.SubscriptionCostValidationException;
import app.service.SubscriptionService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/subscriptions")
@CrossOrigin(origins = "http://localhost:8080")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody SubscriptionDTO subscriptionDTO, Authentication authentication) throws SubscriptionCostValidationException {
        try {
            subscriptionService.createSubscription(subscriptionDTO, authentication);
            return ResponseEntity.ok(new Response<>("", null, 200, true));
        }
        catch (SubscriptionCostValidationException exception) {
            return ResponseEntity.ok(new Response<>("", exception.getMessage(), 409, true));
        }
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", subscriptionService.getAll(), 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSubscription(@PathVariable String id) {
        return ResponseEntity.ok(new Response<>("", subscriptionService.getById(id), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        subscriptionService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
