package app.controllers;

import app.DTOs.Response;
import app.exceptions.UserDoesntExistException;
import app.models.User;
import app.service.CompanyService;
import app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/users")
@CrossOrigin(origins = "http://localhost:8080")
public class UserController {

    private final UserService userService;
    private final CompanyService companyService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", userService.getAll(), 200, true));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> getUser(@PathVariable String id) throws UserDoesntExistException {
        return ResponseEntity.ok(new Response<>("", userService.getById(id), 200, true));
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<?> getUserByEmail(@PathVariable String email) throws UserDoesntExistException {
        return ResponseEntity.ok(new Response<>("", userService.findUser(email), 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        userService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @GetMapping("/getAuthorizedUser")
    public ResponseEntity<?> getAuthorizedUserByToken (Authentication authentication) throws UserDoesntExistException {
        return ResponseEntity.ok(new Response<>("", userService.getByIdentifier(authentication.getName()), 200, true));
    }
    @GetMapping("/getAuthorizedUserWithSubs")
    public ResponseEntity<?> getAuthorizedUserWithSubs (Authentication authentication) throws UserDoesntExistException {
        return ResponseEntity.ok(new Response<>("", userService.getDtoForCabinet(authentication.getName()), 200, true));
    }

    @GetMapping("/getAuthorizedCompanyWithSubs")
    public ResponseEntity<?> getAuthorizedCompanyWithSubs (Authentication authentication) {
        return ResponseEntity.ok(new Response<>("", companyService.getDtoForCompanyCabinet(authentication.getName()), 200, true));
    }

    @GetMapping("/getUserSubscriptions/{userId}")
    public ResponseEntity<?> getUserSubscriptions(@PathVariable(name = "userId") String userId) {
        return ResponseEntity.ok(new Response<>("", userService.getUserSubsById(userId), 200, true));
    }

    @PostMapping("/createAdmin/{identifier}")
    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    public ResponseEntity<?> createAdmin(@PathVariable String identifier) {
        userService.createAdmin(identifier);
        return ResponseEntity.ok(new Response<>("", "Admin with identifier " + identifier + " was created", 200, true));
    }

    @DeleteMapping("/deleteAdmin/{identifier}")
    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    public ResponseEntity<?> deleteAdmin(@PathVariable String identifier) {
        userService.deleteAdmin(identifier);
        return ResponseEntity.ok(new Response<>("", "Admin with identifier " + identifier + " was deleted", 200, true));
    }

    @DeleteMapping("/deleteUser/{identifier}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> deleteUser(@PathVariable String identifier) {
        userService.deleteUser(identifier);
        return ResponseEntity.ok(new Response<>("", "User with identifier " + identifier + " was deleted", 200, true));
    }

    @GetMapping("/getUserRole")
    public ResponseEntity<?> getUserRole (Authentication authentication) throws UserDoesntExistException {
        User user = userService.getByIdentifier(authentication.getName());

        if (user == null) {
            throw new UserDoesntExistException("Пользователь с почтой %s не зарегистрирован".formatted(authentication.getName()));
        }
        return ResponseEntity.ok(new Response<>("", userService.getRole(user), 200, true));
    }
}
