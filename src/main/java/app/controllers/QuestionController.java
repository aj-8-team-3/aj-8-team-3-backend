package app.controllers;


import app.DTOs.QuestionDTO;
import app.DTOs.Response;
import app.models.Question;
import app.service.QuestionService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/questions")
@CrossOrigin(origins = "http://localhost:8080")
public class QuestionController {
    private final QuestionService questionService;


    @GetMapping("/{id}")
    public ResponseEntity<Response> getById(@PathVariable String id){
        Question question = questionService.getById(id);
        return ResponseEntity.ok(new Response<>("", question, 200, true));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response> create(@Valid @RequestBody QuestionDTO questionDTO){
        questionService.create(questionDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response> edit(@Valid @RequestBody QuestionDTO questionDTO, @PathVariable String id){
        questionService.update(id, questionDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable String id){
        questionService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
