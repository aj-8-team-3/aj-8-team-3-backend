package app.controllers;

import app.DTOs.RenderSampleTestVariantDTO;
import app.DTOs.Response;
import app.DTOs.SampleTestDTO;
import app.service.TestVariantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/sample-test")
@CrossOrigin(origins = "http://localhost:8080")
public class SampleTestController {
    private final TestVariantService testVariantService;

    @GetMapping("/{testType}")
    public ResponseEntity<?> getSampleTestForRender(@PathVariable String testType) {
        RenderSampleTestVariantDTO sampleTest = testVariantService.getSampleTestForRender(testType);
        return ResponseEntity.ok(new Response<>("", sampleTest, 200, true));
    }

    @PostMapping("/result")
    public ResponseEntity<?> getSampleTestResult(@RequestBody SampleTestDTO sampleTestDTO) {
        return ResponseEntity.ok(new Response<>("", testVariantService.getSampleTestResult(sampleTestDTO), 200, true));
    }
}
