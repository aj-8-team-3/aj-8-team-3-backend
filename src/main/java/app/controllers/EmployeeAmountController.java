package app.controllers;

import app.DTOs.EmployeeAmountDTO;
import app.DTOs.Response;
import app.service.EmployeeAmountService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/employee-amounts")
@CrossOrigin(origins = "http://localhost:8080")
public class EmployeeAmountController {

    private final EmployeeAmountService employeeAmountService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", employeeAmountService.getAll(), 200, true));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody EmployeeAmountDTO employeeAmountDTO) {
        employeeAmountService.createEmployeeAmount(employeeAmountDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> getEmployeeAmount(@PathVariable String id) {
        return ResponseEntity.ok(new Response<>("", employeeAmountService.getEmployeeAmountById(id), 200, true));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> update(@PathVariable String id, @Valid @RequestBody EmployeeAmountDTO employeeAmountDTO) {
        employeeAmountService.updateEmployeeAmount(id, employeeAmountDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        employeeAmountService.deleteEmployeeAmount(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}

