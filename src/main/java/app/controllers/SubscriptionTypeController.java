package app.controllers;

import app.DTOs.Response;
import app.DTOs.SubscriptionTypeDTO;
import app.service.SubscriptionTypeService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscription_types")
@CrossOrigin(origins = "http://localhost:8080")
public class SubscriptionTypeController {

    private final SubscriptionTypeService subscriptionTypeService;

    public SubscriptionTypeController(SubscriptionTypeService subscriptionTypeService) {
        this.subscriptionTypeService = subscriptionTypeService;
    }

    @GetMapping
    public ResponseEntity<Response<List<SubscriptionTypeDTO>>>getAll(){

        List<SubscriptionTypeDTO> all = subscriptionTypeService.getAll();

        Response<List<SubscriptionTypeDTO>> listResponse = new Response<>(
                                                                            "",
                                                                            all,
                                                                            HttpStatus.OK.value(),
                                                                            true
        );

        return ResponseEntity.ok(listResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<SubscriptionTypeDTO>> getSubscriptionTypeById(@PathVariable String id){
        SubscriptionTypeDTO byId = subscriptionTypeService.getById(id);

        Response<SubscriptionTypeDTO> subscriptionTypeDTOResponse = new Response<>(
                "",
                byId,
                HttpStatus.OK.value(),
                true
        );

        return ResponseEntity.ok(subscriptionTypeDTOResponse);
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response<SubscriptionTypeDTO>> create(@Valid @RequestBody SubscriptionTypeDTO subscriptionTypeDTO){

        SubscriptionTypeDTO saved = subscriptionTypeService.create(subscriptionTypeDTO);

        Response<SubscriptionTypeDTO> response = new Response<>(
                "",
                saved,
                HttpStatus.CREATED.value(),
                true
        );

        return ResponseEntity.ok(response);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response<Object>> delete(@PathVariable String id){

        subscriptionTypeService.delete(id);

        Response<Object> response = new Response<>(
                "Deleted successfully",
                null,
                HttpStatus.OK.value(),
                true
        );

        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Response<SubscriptionTypeDTO>> update(@Valid @RequestBody SubscriptionTypeDTO subscriptionTypeDTO,
                                              @PathVariable String id){

        SubscriptionTypeDTO updated = subscriptionTypeService.update(subscriptionTypeDTO, id);

        Response<SubscriptionTypeDTO> response = new Response<>(
                "",
                updated,
                HttpStatus.OK.value(),
                true
        );

        return ResponseEntity.ok(response);
    }
}