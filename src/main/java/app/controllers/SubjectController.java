package app.controllers;

import app.DTOs.Response;
import app.DTOs.SubjectDTO;
import app.models.Subject;
import app.service.SubjectService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/subjects")
@CrossOrigin(origins = "http://localhost:8080")
public class SubjectController {

    private final SubjectService subjectService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(new Response<>("", subjectService.getAll(), 200, true));
    }

    @GetMapping("/by-test-type/{testType}")
    public ResponseEntity<?> getAllByTestType(@PathVariable String testType) {
        List<Subject> subjects = subjectService.getSubjectsByTestName(testType);
        return ResponseEntity.ok(new Response<>("", subjects, 200, true));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSubject(@PathVariable String id) {
        return ResponseEntity.ok(new Response<>("", subjectService.getById(id), 200, true));
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody SubjectDTO subjectDTO) {
        subjectService.create(subjectDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable String id) {
        subjectService.delete(id);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> update(@PathVariable String id, SubjectDTO subjectDTO) {
        subjectService.update(id, subjectDTO);
        return ResponseEntity.ok(new Response<>("", null, 200, true));
    }
}
