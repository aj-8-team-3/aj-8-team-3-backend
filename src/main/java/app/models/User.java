package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document("users")
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    private String id;

    @Indexed(unique = true)
    private String identifier;

    private String password;

    private Boolean verified = false;

    private LocalDate registrationDate = LocalDate.now();

    public void setId(UUIDGenerator<User> generator) {
        this.id = generator.generate(this);
    }
}
