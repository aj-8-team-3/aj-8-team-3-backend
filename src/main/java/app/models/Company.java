package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("companies")
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Company {

    @Id
    private String id;

    private String ownerId;

    private List<User> employees;

    public void setId(UUIDGenerator<Company> generator) {
        this.id = generator.generate(this);
    }
}
