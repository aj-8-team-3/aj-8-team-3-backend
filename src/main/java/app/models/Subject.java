package app.models;

import app.utils.UUIDGenerator;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonTypeResolver;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("subjects")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Subject {

    @Id
    private String id;

    private String title;

    private TestType testType;

    private List<String> dependencies;

    private boolean isRequired;

    public void setId(UUIDGenerator<Subject> generator) {
        this.id = generator.generate(this);
    }
}
