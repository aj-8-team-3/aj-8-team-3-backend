package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

@Document("authorities")
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Authority implements GrantedAuthority {
    @Id
    String id;

    @Indexed
    String authority;

    public void setId(UUIDGenerator<Authority> generator) {
        this.id = generator.generate(this);
    }
}
