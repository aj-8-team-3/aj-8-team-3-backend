package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("test_variants")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestVariant {

    @Id
    private String id;

    private String testTypeId;

    private String description;

    private List<Subject> subjectList;

    private String language;

    public void setId(UUIDGenerator<TestVariant> generator) {
        this.id = generator.generate(this);
    }
}
