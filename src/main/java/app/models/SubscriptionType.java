package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("subscription_type")
public class SubscriptionType {

    @Id
    private  String id;
    private int  duration;
    private int price;
    public void setId(UUIDGenerator<SubscriptionType> generator) {
        this.id = generator.generate(this);
    }
}


