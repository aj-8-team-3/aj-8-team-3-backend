package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("additions")
public class Addition {
    @Id
    private String id;
    private String testId;
    private String subjectId;
    private String type;
    private String contentText;
    private String contentFile;

    public void setId(UUIDGenerator<Addition> generator) {
        this.id = generator.generate(this);
    }
}
