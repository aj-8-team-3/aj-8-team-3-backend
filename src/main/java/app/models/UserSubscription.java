package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("users_subscriptions")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserSubscription {

    @Id
    private String id;

    private String userId;

    private List<Subscription> subscriptions;

    public void setId(UUIDGenerator<UserSubscription> generator) {
        this.id = generator.generate(this);
    }
}
