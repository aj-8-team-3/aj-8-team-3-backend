package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("user_authorities")
@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthority {

    @Id
    private String id;
    private String userId;
    private List<Authority> authorities;

    public void setId(UUIDGenerator<UserAuthority> generator) {
        this.id = generator.generate(this);
    }
}
