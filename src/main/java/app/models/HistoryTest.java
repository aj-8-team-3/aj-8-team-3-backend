package app.models;

import app.utils.UUIDGenerator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Getter @Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("history_tests")
public class HistoryTest {

    @Id
    private String id;

    private String userId;

    private String testId;

    private List<String> subjectsList;

    private int result;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime submissionDate;

    public void setId(UUIDGenerator<HistoryTest> generator) {
        this.id = generator.generate(this);
    }
}
