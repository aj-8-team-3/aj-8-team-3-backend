package app.models;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("employee_amount")
public class EmployeeAmount {
    private String id;
    private int employeeAmount;
    private int discount;
}
