package app.models;

import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter @Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("history_questions")
public class HistoryQuestion {

    @Id
    private String id;

    private String HistoryTestId;

    private String questionId;

    private List<String> chosenAnswers;

    public void setId(UUIDGenerator<HistoryQuestion> generator) {
        this.id = generator.generate(this);
    }
}
