package app.models;


import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("test_types")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestType {

    @Id
    private String id;

    private String title;

    private Integer subscriptionPrice;

    private Integer instantPrice;
    private Boolean isMultiLang;

    public void setId(UUIDGenerator<TestType> generator) {
        this.id = generator.generate(this);
    }
}
