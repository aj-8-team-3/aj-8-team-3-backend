package app.models;

import app.utils.UUIDGenerator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document("subscriptions")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Subscription {

    @Id
    private String id;

    private List<TestType> testTypes;

    private SubscriptionType subscriptionType;

    private Integer employeesAmount;

    private Integer currentEmployees;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime buyingDate;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime expireDate;

    private Integer cost;

    public void setId(UUIDGenerator<Subscription> generator) {
        this.id = generator.generate(this);
    }
}
