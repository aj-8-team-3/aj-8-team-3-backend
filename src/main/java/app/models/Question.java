package app.models;


import app.utils.UUIDGenerator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("questions")
public class Question {
    @Id
    private String id;
    private String testId;
    private String subjectId;
    private String title;
    private List<String> answers;
    private List<String> rightAnswers;

    public void setId(UUIDGenerator<Question> generator) {
        this.id = generator.generate(this);
    }
}
