package app.service;

import app.exceptions.UserDoesntExistException;
import app.models.Company;
import app.models.Subscription;
import app.models.User;
import app.models.UserSubscription;
import app.repositories.CompanyRepository;
import app.service.impl.CompanyServiceImpl;
import app.service.impl.SubscriptionServiceImpl;
import app.service.impl.UserServiceImpl;
import app.service.impl.UserSubscriptionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {
    @InjectMocks
    private CompanyServiceImpl companyService;
    @Mock
    private SubscriptionServiceImpl subscriptionService;
    @Mock
    private UserSubscriptionServiceImpl userSubscriptionService;
    @Mock
    private UserServiceImpl userService;
    @Mock
    private CompanyRepository companyRepository;
    @Test
    void addUser() throws UserDoesntExistException {
        List<String> subsToGive = List.of("sub1_id", "sub2_id");
        Company company = new Company("company_id", "owner_id", new ArrayList<>());
        User employee = new User("employee_id", "employee@gmail.com", "password", true, LocalDate.now());
        UserSubscription employeeSubscription = new UserSubscription("id", "emp_id", new ArrayList<>());
        List<Subscription> companySubs = new ArrayList<>();
        companySubs.add(new Subscription("sub1_id", null, null, null, 0, null, null, null));
        companySubs.add(new Subscription("sub2_id", null, null, null, 2, null, null, null));
        UserSubscription companySubscription = new UserSubscription("id", "ownerId", companySubs);

        Mockito.when(companyRepository.findById(Mockito.anyString())).thenReturn(Optional.of(company));
        Mockito.when(userService.getById("employee_id")).thenReturn(employee);
        when(userSubscriptionService.getByUserId("employee_id")).thenReturn(employeeSubscription);
        when(userSubscriptionService.getByUserId("owner_id")).thenReturn(companySubscription);

        ArgumentCaptor<Subscription> captor = ArgumentCaptor.forClass(Subscription.class);

        companyService.addUser("company_id", "employee_id", subsToGive);

        verify(subscriptionService, times(2)).save(captor.capture());
        List<Subscription> capturedEmployeeSubs = captor.getAllValues();

        Assertions.assertEquals(2, capturedEmployeeSubs.size());
        Assertions.assertTrue(capturedEmployeeSubs.stream().map(sub -> sub.getId()).collect(Collectors.toList()).containsAll(subsToGive));
        Assertions.assertEquals(1, capturedEmployeeSubs.get(0).getCurrentEmployees());
        Assertions.assertEquals(3, capturedEmployeeSubs.get(1).getCurrentEmployees());
    }
}
