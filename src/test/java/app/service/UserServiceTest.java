package app.service;

import app.DTOs.PasswordDTO;
import app.configuration.tokenProviders.MailValidationTokenProvider;
import app.configuration.tokenProviders.PasswordResetTokenProvider;
import app.exceptions.MessageSendlerException;
import app.exceptions.UserDoesntExistException;
import app.models.User;
import app.repositories.UserRepository;
import app.service.impl.MailSenderService;
import app.service.impl.UserServiceImpl;
import jakarta.mail.MessagingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private MailValidationTokenProvider mailValidationTokenProvider;
    @Mock
    private PasswordResetTokenProvider passwordResetTokenProvider;
    @Mock
    private MailSenderService mailSender;
    @Mock
    private UserRepository userRepository;

    @Test
    public void createMailValidationTokenTest() throws MessageSendlerException, MessagingException, IOException {
        String token = "mailValidationToken";
        Mockito.when(mailValidationTokenProvider.createMailValidationToken("user@gmail.com")).thenReturn(token);

        userService.createMailValidationToken("user@gmail.com");

        verify(mailSender, times(1)).sendEmailWithHtmlTemplate(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
    }
    @Test
    public void createPasswordResetTokenForUserTest() throws MessageSendlerException, MessagingException, IOException {
        String passwordResetToken = "token";
        Mockito.when(passwordResetTokenProvider.createPasswordResetToken("user@gmail.com")).thenReturn(passwordResetToken);
        userService.createPasswordResetTokenForUser(new User("id", "user@gmail.com", "passwrod", true, Mockito.any()));

        verify(mailSender, times(1)).sendEmailWithHtmlTemplate(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
    }
    @Test
    public void resetPassword_userNotFound() throws UserDoesntExistException {
        User user = null;
        when(userRepository.findByIdentifier("user@gmail.com")).thenReturn(user);

        Assertions.assertThrows(UserDoesntExistException.class, () -> userService.resetPassword(new PasswordDTO()));
    }
}
