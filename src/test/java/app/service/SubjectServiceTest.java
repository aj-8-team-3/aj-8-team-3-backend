package app.service;


import app.models.Subject;
import app.models.TestType;
import app.repositories.SubjectRepository;
import app.repositories.TestTypeRepository;
import app.service.impl.SubjectServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.apache.logging.log4j.Logger;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class SubjectServiceTest {
    @InjectMocks
    private SubjectServiceImpl subjectService;
    @Mock
    private SubjectRepository subjectRepository;
    @Mock
    private TestTypeRepository testTypeRepository;
    @Mock
    private Logger logger;
    @Test
    public void getSubjectsByTestName() {
        TestType testType = new TestType("id", "ent", 0, 0, true);
        Mockito.when(testTypeRepository.findTestTypeByTitle(Mockito.anyString())).thenReturn(testType);

        List<Subject> subjects = subjectService.getSubjectsByTestName("ent");

        Assertions.assertEquals(new ArrayList<>(), subjects);
    }
    @Test
    public void getSubjectsByTestName_testTypeNull() {
        TestType testType = null;
        Mockito.when(testTypeRepository.findTestTypeByTitle(Mockito.anyString())).thenReturn(testType);


        Assertions.assertThrows(NoSuchElementException.class, () -> subjectService.getSubjectsByTestName("ent"));
    }
}
