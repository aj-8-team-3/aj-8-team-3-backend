package app.testType;

import app.DTOs.TestTypeDTO;
import app.models.TestType;
import app.repositories.TestTypeRepository;
import app.service.impl.TestTypeServiceImpl;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class TestTypeServiceImplTest {

    @Mock
    private TestTypeRepository testTypeRepository;

    @Mock
    private Logger logger;

    @InjectMocks
    private TestTypeServiceImpl testTypeService;

    private TestTypeDTO testTypeDTO;
    private TestType testType;

    @BeforeEach
    void setUp() {
        testTypeDTO = new TestTypeDTO("1", "Test Title", 100, 50, false);
        testType = new TestType("1", "Test Title", 100, 50, false);
    }

    @Test
    void testGetAllTestTypeDTOs() {
        List<TestType> testTypes = List.of(testType);
        Mockito.when(testTypeRepository.findAll()).thenReturn(testTypes);

        List<TestTypeDTO> result = testTypeService.getAllTestTypeDTOs();

        assertEquals(testTypes.size(), result.size());
        for (int i = 0; i < testTypes.size(); i++) {
            TestTypeDTO expectedDTO = TestTypeDTO.fromTestType(testTypes.get(i));
            TestTypeDTO actualDTO = result.get(i);
            assertEquals(expectedDTO.getId(), actualDTO.getId());
            assertEquals(expectedDTO.getTitle(), actualDTO.getTitle());
            assertEquals(expectedDTO.getSubscriptionPrice(), actualDTO.getSubscriptionPrice());
            assertEquals(expectedDTO.getInstantPrice(), actualDTO.getInstantPrice());
            assertEquals(expectedDTO.getIsMultiLang(), actualDTO.getIsMultiLang());
        }
    }

    @Test
    void testGetByIdFound() {
        String id = "1";
        Mockito.when(testTypeRepository.findById(id)).thenReturn(Optional.of(testType));

        TestType result = testTypeService.getById(id);

        assertEquals(testType, result);
    }

    @Test
    void testGetByIdNotFound() {
        String id = "1";
        Mockito.when(testTypeRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> testTypeService.getById(id));
    }

    @Test
    void testDelete() {
        String id = "1";
        testTypeService.delete(id);

        Mockito.verify(testTypeRepository, Mockito.times(1)).deleteById(id);
        Mockito.verify(logger, Mockito.times(1)).info("Вид теста с id:{} был удален из базы", id);
    }

    @Test
    void testUpdateFound() {
        String id = "1";
        TestType updatedTestType = new TestType(id, "Updated Title", 200, 100, true);
        Mockito.when(testTypeRepository.findById(id)).thenReturn(Optional.of(testType));

        testTypeService.update(id, TestTypeDTO.fromTestType(updatedTestType));

        assertEquals("Updated Title", testType.getTitle());
        assertEquals(200, testType.getSubscriptionPrice());
        assertEquals(100, testType.getInstantPrice());

        Mockito.verify(testTypeRepository, Mockito.times(1)).save(testType);
        Mockito.verify(logger, Mockito.times(1)).info("Тест с id:{} обновлен", id);
    }


    @Test
    void testUpdateNotFound() {
        String id = "1";
        TestType updatedTestType = new TestType(id, "Updated Title", 200, 100, true);
        Mockito.when(testTypeRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> testTypeService.update(id, TestTypeDTO.fromTestType(updatedTestType)));
    }
}
