package app.historyTest;

import app.DTOs.UserHistoryTestDTO;
import app.models.HistoryTest;
import app.repositories.HistoryTestRepository;
import app.repositories.TestVariantRepository;
import app.service.impl.HistoryTestServiceImpl;
import app.utils.UUIDGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

class HistoryTestServiceImplTest {

    @Mock
    private HistoryTestRepository historyTestRepository;

    @Mock
    private TestVariantRepository testVariantRepository;

    @Mock
    private UUIDGenerator generator;

    @Mock
    private org.apache.logging.log4j.Logger logger;
    @InjectMocks
    private HistoryTestServiceImpl historyTestService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAll() {
        List<HistoryTest> expectedHistoryTests = new ArrayList<>();
        Mockito.when(historyTestRepository.findAll()).thenReturn(expectedHistoryTests);

        List<HistoryTest> result = historyTestService.getAll();

        assertEquals(expectedHistoryTests, result);
    }

    @Test
    void testGetAllByUserId() {
        String userId = "user123";
        List<HistoryTest> historyTests = new ArrayList<>();
        Mockito.when(historyTestRepository.findAllByUserId(userId)).thenReturn(historyTests);
        Mockito.when(testVariantRepository.findTestVariantById(anyString())).thenReturn(null);

        List<UserHistoryTestDTO> result = historyTestService.getAllByUserId(userId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void testGetByIdFound() {
        String id = "123";
        HistoryTest historyTest = new HistoryTest();
        Mockito.when(historyTestRepository.findById(id)).thenReturn(Optional.of(historyTest));

        HistoryTest result = historyTestService.getById(id);

        assertEquals(historyTest, result);
    }

    @Test
    void testGetByIdNotFound() {
        String id = "123";
        Mockito.when(historyTestRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> historyTestService.getById(id));
    }

    @Test
    void testSave() {
        HistoryTest historyTest = new HistoryTest();

        historyTestService.save(historyTest);

        Mockito.verify(historyTestRepository, Mockito.times(1)).save(historyTest);
    }

    @Test
    void testDelete() {
        String id = "test123";

        historyTestService.delete(id);

        Mockito.verify(historyTestRepository, Mockito.times(1)).deleteById(id);
        Mockito.verify(logger, Mockito.times(1)).info("Тест id:{} удален из базы", id);
    }

}