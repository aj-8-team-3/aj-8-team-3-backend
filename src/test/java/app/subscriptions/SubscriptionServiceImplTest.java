package app.subscriptions;
import app.models.Subscription;
import app.models.SubscriptionType;
import app.models.TestType;
import app.repositories.*;
import app.service.impl.SubscriptionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith({MockitoExtension.class})
public class SubscriptionServiceImplTest {

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @InjectMocks
    private SubscriptionServiceImpl subscriptionService;

    @Test
    void get_all_subscriptions_test() {
        List<Subscription> subscriptions = getSubscriptions();

        Mockito.when(subscriptionRepository.findAll()).thenReturn(subscriptions);

        List<Subscription> result = subscriptionService.getAll();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(subscriptions, result);
    }

    private List<Subscription> getSubscriptions() {
        Subscription firstSubscription = new Subscription(
                                                        "1",
                                                        getTestTypes(),
                                                        new SubscriptionType("1", 10, 100),
                                                        0,
                                                        10,
                                                        LocalDateTime.now(),
                                                        LocalDateTime.now(),
                                                        150);

        Subscription secondSubscription = new Subscription(
                                                        "2",
                                                        getTestTypes(),
                                                        new SubscriptionType("2", 15, 150),
                                                        2,
                                                        15,
                                                        LocalDateTime.now(),
                                                        LocalDateTime.now(),
                                                        200
        );

        return List.of(firstSubscription, secondSubscription);
    }

    private List<TestType> getTestTypes() {
        TestType testType = new TestType();

        testType.setTitle("test");
        testType.setSubscriptionPrice(100);
        testType.setInstantPrice(50);

        return List.of(testType);
    }
}
