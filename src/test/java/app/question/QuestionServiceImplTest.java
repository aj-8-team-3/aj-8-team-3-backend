package app.question;

import app.DTOs.QuestionDTO;
import app.models.Question;
import app.repositories.QuestionRepository;
import app.service.impl.QuestionServiceImpl;
import app.utils.UUIDGenerator;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplTest {

    @Mock
    private QuestionRepository repository;

    @Mock
    private UUIDGenerator generator;

    @Mock
    private Logger logger;

    @InjectMocks
    private QuestionServiceImpl questionService;

    private QuestionDTO questionDTO;

    private Question question;

    @BeforeEach
    void setUp() {
        questionDTO = QuestionDTO.builder()
                .title("Test Question")
                .subjectId("123")
                .testId("456")
                .answers(List.of("Answer 1", "Answer 2"))
                .rightAnswers(List.of("Answer 1"))
                .build();

        question = Question.builder()
                .title("Test Question")
                .subjectId("123")
                .testId("456")
                .answers(List.of("Answer 1", "Answer 2"))
                .rightAnswers(List.of("Answer 1"))
                .build();
    }

    @Test
    void testGetAllQuestions() {
        List<Question> questions = List.of(question);
        Mockito.when(repository.findAll()).thenReturn(questions);

        List<Question> result = questionService.getAll();

        Assertions.assertEquals(questions, result);
    }

    @Test
    void testGetQuestionById() {
        String id = "123";
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(question));

        Question result = questionService.getById(id);

        Assertions.assertEquals(question, result);
    }

    @Test
    void testGetQuestionByIdNotFound() {
        String id = "123";
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        Assertions.assertThrows(NoSuchElementException.class, () -> {
            questionService.getById(id);
        });
    }

    @Test
    void testUpdateQuestion() {
        String id = "123";
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(question));
        questionService.update(id, questionDTO);
        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(logger, Mockito.times(1)).info(Mockito.eq("Вопрос с ID: {} обновлен"), Mockito.eq("123"));
    }

    @Test
    void testDeleteQuestion() {
        String id = "123";
        questionService.delete(id);
        Mockito.verify(repository, Mockito.times(1)).deleteById(id);
        Mockito.verify(logger, Mockito.times(1)).info(Mockito.eq("Вопрос с ID:{} удален из базы"), Mockito.eq("123"));
    }
}
