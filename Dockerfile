FROM maven:3.8-openjdk-17-slim as build

COPY ./ /usr/src/app
WORKDIR /usr/src/app

RUN mvn clean package -DskipTests -X

RUN ls /usr/src/app/target/


FROM openjdk:17-jdk-slim

WORKDIR /usr/app

COPY --from=build /usr/src/app/target/ .

EXPOSE 8000

CMD ["java", "-jar", "learnAI-0.0.1-SNAPSHOT.jar"]